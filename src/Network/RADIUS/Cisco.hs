{-# LANGUAGE OverloadedStrings #-}

module Network.RADIUS.Cisco
  ( avPair
  )
where

import Data.ByteString

import Network.RADIUS.Types (Attribute(..), VSA(..))

avPair :: ByteString -> Attribute
avPair = AttrVendorSpecific . VSACisco

{-

data CiscoProto
  = CiscoProtoIP
  | CiscoProtoIPX
  | CiscoProtoVPDN
  | CiscoProtoVOIP
  | CiscoProtoSHELL
  | CiscoProtoRSVP
  | CiscoProtoSIP
  | CiscoProtoAIRNET
  | CiscoProtoOUTBOUND
  deriving (Show, Eq)

ser :: CiscoProto -> ByteString
ser s = case s of
  CiscoProtoIP       -> "ip"
  CiscoProtoIPX      -> "ipx"
  CiscoProtoVPDN     -> "vpdn"
  CiscoProtoVOIP     -> "voip"
  CiscoProtoSHELL    -> "shell"
  CiscoProtoRSVP     -> "rvsp"
  CiscoProtoSIP      -> "sip"
  CiscoProtoAIRNET   -> "airnet"
  CiscoProtoOUTBOUND -> "output"

deser :: (MonadFail f) => ByteString -> f CiscoProto
deser s = case s of
  "ip"       -> pure CiscoProtoIP
  "ipx"      -> pure CiscoProtoIPX
  "vpdn"     -> pure CiscoProtoVPDN
  "voip"     -> pure CiscoProtoVOIP
  "shell"    -> pure CiscoProtoSHELL
  "rsvp"     -> pure CiscoProtoRSVP
  "sip"      -> pure CiscoProtoSIP
  "airnet"   -> pure CiscoProtoAIRNET
  "outbound" -> pure CiscoProtoOUTBOUND
  _          -> fail ("Unknown Cisco-RADIUS Protocol: " <> show s)

-}

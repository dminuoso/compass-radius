{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TypeApplications           #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-|
Module      : Network.RADIUS.Encoding
Description :
Copyright   : (c) Victor Nawothnig, 2018
License     : BSD3
Maintainer  : victor.nawothnig@wobcom.de
Stability   : experimental
Portability : POSIX

This module provides Serialize instances for the RADIUS Packet type and attributes. So you basically decode a (lazy) ByteString and get a RADIUS Packet back or you can encode a RADIUS Packet to a ByteString, which you can then send on the wire as is, etc. Simple as that.

-}

module Network.RADIUS.Encoding
  ( encodePacket
  , decodePacket
  , getRL
  , putAttribute
  , getAttribute
  , RadiusSer(..)
  , module Network.RADIUS.Crypto
  )
where

import           Control.Monad (MonadPlus, mplus, mzero, replicateM, when)
import           Control.Monad.Fail (MonadFail)
import           Data.Foldable (traverse_)
import           Data.Int
import           Data.Semigroup ((<>))
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import           Text.Read (readMaybe)

import           Data.Word
import           GHC.Generics (from, to)

import           Data.Serialize

import qualified Data.ByteString as S
import           Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as SBS
import qualified Data.ByteString.Lazy as L
import           Data.IP
import           Data.Time.Clock

import           Network.RADIUS.Crypto
import           Network.RADIUS.Types


whileM :: (Monad m, MonadPlus f) => m Bool -> m a -> m (f a)
whileM p f = go
    where go = do
            x <- p
            if x
                then do
                        x'  <- f
                        xs <- go
                        return (return x' `mplus` xs)
                else return mzero

class RadiusSer t where
    putR :: Putter t
    getR :: Int -> Get t

-- | Get the length and then read a RadiusSer type.
getRL :: RadiusSer a => Get a
getRL = getR =<< getLength

getLength :: Integral i => Get i
getLength = subtract 2 <$> getWord8I

getWord8I :: Integral i => Get i
getWord8I = fromIntegral <$> getWord8

instance RadiusSer T.Text where
    putR = putR . T.encodeUtf8
    getR l = do
      bs <- getR l
      case T.decodeUtf8' bs of
        Left err -> fail ("Failed to decode UTF-8: " <> show err)
        Right t  -> pure t

instance RadiusSer [Char] where
    putR = putR . T.pack
    getR l = T.unpack <$> getR l

instance RadiusSer S.ByteString where
    putR bs = do
      putLength (fromIntegral (S.length bs))
      putByteString bs
    getR l = getByteString l

instance RadiusSer L.ByteString where
    putR bs = do
      putLength (fromIntegral (L.length bs))
      putLazyByteString bs
    getR l = getLazyByteString (fromIntegral l)

instance RadiusSer IPv4 where
    putR i = putLength 4 >> gPut (from i)
    getR l = label "IPv4" (l `shouldBe` 4 *> (to <$> gGet))

instance RadiusSer IPv6 where
    putR i = putLength 16 >> gPut (from i)
    getR l = do
      bs <- getByteString l
      case readMaybe (SBS.unpack bs) of
        Nothing -> fail "Failed to parse IPv6 address"
        Just x  -> pure x

instance RadiusSer (AddrRange IPv4) where
    putR r = do
      -- 4 byte prefix, 1 byte reserved
      putLength (4 + 1)
      let (addr, prefLen) = addrRangePair r
      -- Reserved byte
      putWord8 0

      putWord8 (fromIntegral prefLen)
      traverse_ (putWord8 . fromIntegral) (fromIPv4 addr)

    getR l = do
       _reserved <- getWord8
       prefLen <- fromIntegral <$> getWord8

       -- Subtract the Reserved/Prefix-Length header
       let nBytes = l - 2

       bytes <- replicateM nBytes (fromIntegral <$> getWord8)
       let addr = take 4 (bytes ++ repeat 0)
       let prefix = makeAddrRange (toIPv4 addr) prefLen
       pure prefix

instance RadiusSer (AddrRange IPv6) where
    putR r = putR payload
      where
        (addr, prefLen) = addrRangePair r

        payload :: ByteString
        payload = runPut $ do
          putWord8 0 -- Reserved byte
          putWord8 (fromIntegral prefLen)
          traverse_ (putWord8 . fromIntegral) (fromIPv6b addr)

    getR l = do
       _reserved <- getWord8
       prefLen <- fromIntegral <$> getWord8

       -- Subtract the Reserved/Prefix-Length header
       let nBytes = l - 2

       bytes <- replicateM nBytes (fromIntegral <$> getWord8)
       let addr = take 16 (bytes ++ repeat 0)
       let prefix = makeAddrRange (toIPv6b addr) prefLen
       pure prefix

shouldBe :: MonadFail m => Int -> Int -> m ()
shouldBe l n = () <$ do
  when (l /= n ) (fail (concat [ "incorrect length"
                               , ", expected it to be " <> show (n + 2)
                               , ", but actual length is " <> show (l + 2)
                               ]))
instance RadiusSer NominalDiffTime where
    putR = putR @Word32 . round . nominalDiffTimeToSeconds
    getR l = toNom <$> getR l
      where
        toNom :: Word32 -> NominalDiffTime
        toNom = secondsToNominalDiffTime . fromIntegral

instance RadiusSer Int16 where
    putR i = putLength 2 >> putInt16be i
    getR l = l `shouldBe` 2 >> getInt16be

instance RadiusSer Int32 where
    putR i = putLength 4 >> putInt32be i
    getR l = l `shouldBe` 4 >> getInt32be

instance RadiusSer Int64 where
    putR i = putLength 8 >> putInt64be i
    getR l = l `shouldBe` 8 >> getInt64be

instance RadiusSer Word16 where
    putR i = putLength 2 >> putWord16be i
    getR l = l `shouldBe` 2 >> getWord16be

instance RadiusSer Word32 where
    putR i = putLength 4 >> putWord32be i
    getR l = l `shouldBe` 4 >> getWord32be

instance RadiusSer Word64 where
    putR i = putLength 8 >> putWord64be i
    getR l = l `shouldBe` 8 >> getWord64be

instance RadiusSer IngressFiltersState where
    putR = putNumSer32
    getR = getNumSer32

instance RadiusSer AcctStatusType where
    putR = putNumSer32
    getR l = label "AcctStatusType" (getNumSer32 l)

instance RadiusSer Authentic where
    putR = putNumSer32
    getR l = label "Authentic" (getNumSer32 l)

instance RadiusSer TerminateCause where
    putR = putNumSer32
    getR l = label "TerminateCause" (getNumSer32 l)

instance RadiusSer ServiceType where
    putR = putNumSer32
    getR l = label "ServiceType" (getNumSer32 l)

instance RadiusSer FramedProtocol where
    putR = putNumSer32
    getR l = label "FramedProtocol" (getNumSer32 l)

instance RadiusSer FramedRouting where
    putR = putNumSer32
    getR l = label "FramedRouting" (getNumSer32 l)

instance RadiusSer FramedCompression where
    putR = putNumSer32
    getR l = label "FramedCompression" (getNumSer32 l)

instance RadiusSer LoginService where
    putR = putNumSer32
    getR l = label "LoginService" (getNumSer32 l)

instance RadiusSer TerminationAction where
    putR = putNumSer32
    getR l = label "TerminationAction" (getNumSer32 l)

instance RadiusSer NASPortType where
    putR = putNumSer32
    getR l = label "NASPortType" (getNumSer32 l)

instance RadiusSer ARAPZoneAccess where
    putR = putNumSer32
    getR l = label "ARAPZoneAccess" (getNumSer32 l)

instance RadiusSer TunnelType where
    putR = putNumSer32
    getR l = label "TunnelType" (getNumSer32 l)

instance RadiusSer TunnelMediumType where
    putR = putNumSer32
    getR l = label "TunnelMediumType" (getNumSer32 l)

decodePacket :: ByteString -> Either String Packet
decodePacket = runGet getPacket

encodePacket :: Packet -> ByteString
encodePacket = runPut . putPacket

putPacket :: Putter Packet
putPacket Packet{..} = do
    put packetType
    putWord8 packetId
    putWord16be (attrsLen + 20)
    putByteString packetAuthenticator
    putByteString attrs
  where
    attrs = runPut (traverse_ putAttribute packetAttributes)
    attrsLen   = fromIntegral (SBS.length attrs)

getPacket :: Get Packet
getPacket = do
      packetType          <- get
      packetId            <- getWord8
      packetLength        <- getWord16be
      packetAuthenticator <- getByteString 16

      packetAttributes    <- whileM someLeft getAttribute
      pure Packet{..}
    where
      someLeft :: Get Bool
      someLeft = not <$> isEmpty
instance Serialize PacketType where
  get = getWord8 >>= deserialize
  put = putWord8 . serialize

-- | Put a RADIUS type and then some more.
putRTy :: RadiusSer s => Word8 -> Putter s
putRTy i v = putWord8 i >> putR v

-- | Put the length. This adds an offset to accout for both the type and length tag itself.
putLength :: Putter Word8
putLength = putWord8 . (+ 2)

-- | Put a RADIUS enum.
putNumSer32 :: NumSerializable n => Putter n
putNumSer32 x = do
  putLength 4
  putWord32be (serialize x)

-- | Get a RADIUS enum.
getNumSer32 :: NumSerializable n => Int -> Get n
getNumSer32 l = do
  deserialize =<< getWord32be

-- | Reads an entire RAIDUS attribute, including type and length fields.
getAttribute :: Get Attribute
getAttribute = do
  ty <- getWord8
  le <- getWord8
  getAttrComp (fromIntegral le - 2) ty

-- | Puts an entire RADIUS attribute, including type and length fields.
putAttribute :: Putter Attribute
-- 1: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.1)
putAttribute (AttrUserName s)          = putRTy 1 s

-- 2: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.2)
putAttribute (AttrUserPassword s)      = putRTy 2 s

-- 3: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.3)
putAttribute (AttrCHAPPassword ident s)      = do
    let len = 3 + (SBS.length s)

    putWord8 3
    putWord8 (fromIntegral len)
    putWord8 ident
    putByteString s

-- 4: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.4)
putAttribute (AttrNASIPAddress s)      = putRTy 4 s

-- 5: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.5)
putAttribute (AttrNASPort s)           = putRTy 5 s

-- 6: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.6)
putAttribute (AttrServiceType s)       = putRTy 6 s

-- 7: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.7)
putAttribute (AttrFramedProtocol s)    = putRTy 7 s

-- 8: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.8)
putAttribute (AttrFramedIPAddress s)   = putRTy 8 s

-- 9: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.9)
putAttribute (AttrFramedIPNetmask s)   = putRTy 9 s

-- 10: [rfc2865](https://tools.ietf.org/html/rfc2865#section-5.10)
putAttribute (AttrFramedRouting s)     = putRTy 10 s

-- 11: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.11)
putAttribute (AttrFilterId s)          = putRTy 11 s

-- 12: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.12)
putAttribute (AttrFramedMTU s)         = putRTy 12 s

-- 13: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.13)
putAttribute (AttrFramedCompression s) = putRTy 13 s

-- 14: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.14)
putAttribute (AttrLoginIPHost s)       = putRTy 14 s

-- 15: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.15)
putAttribute (AttrLoginService s)      = putRTy 15 s

-- 16: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.16)
putAttribute (AttrLoginTCPPort s)      = putRTy 16 s

-- 17 is unassigned


-- 18: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.18)
putAttribute (AttrReplyMessage s)      = putRTy 18 s

-- 19: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.19)
putAttribute (AttrCallbackNumber s)    = putRTy 19 s

-- 20: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.20)
putAttribute (AttrCallbackId s)        = putRTy 20 s

-- 21 is unassigned

-- 22: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.22)
putAttribute (AttrFramedRoute s)       = putRTy 22 s

-- 23: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.23)
putAttribute (AttrFramedIPXNetwork s)  = putRTy 23 s

-- 24: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.24)
putAttribute (AttrState s)             = putRTy 24 s

-- 25: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.25)
putAttribute (AttrClass s)             = putRTy 25 s

-- 26: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.26)
putAttribute (AttrVendorSpecific s) = do
    putWord8 26
    case s of
        VSACisco str -> do
            let payload :: ByteString
                payload = runPut $ do
                    -- Cisco Vendor ID
                    putWord32be 9

                    -- Cisco Vendor Type (always 1)
                    putWord8 1

                    let len = (SBS.length str + 2)
                    putWord8 . fromIntegral $ len

                    putByteString str

            putWord8 . fromIntegral $ (SBS.length payload + 2)
            putByteString payload
        _ -> error "Unknown VSA received"

-- 27: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.27)
putAttribute (AttrSessionTimeout s)    = putRTy 27 s

-- 28: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.28)
putAttribute (AttrIdleTimeout s)       = putRTy 28 s

-- 29: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.29)
putAttribute (AttrTerminationAction s) = putRTy 29 s

-- 30: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.30)
putAttribute (AttrCalledStationId s)   = putRTy 30 s

-- 31: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.31)
putAttribute (AttrCallingStationId s)  = putRTy 31 s

-- 32: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.32)
putAttribute (AttrNASIdentifier s)     = putRTy 32 s

-- 33: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.33)
putAttribute (AttrProxyState s)        = putRTy 33 s

-- 34: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.34)
putAttribute (AttrLoginLATService s)   = putRTy 34 s

-- 35: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.35)
putAttribute (AttrLoginLATNode s)      = putRTy 35 s

-- 36: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.36)
putAttribute (AttrLoginLATGroup s)     = putRTy 36 s

-- 37: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.37)
putAttribute (AttrFramedAppleTalkLink s)    = putRTy 37 s

-- 38: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.38)
putAttribute (AttrFramedAppleTalkNetwork s) = putRTy 38 s

-- 39: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.39)
putAttribute (AttrFramedAppleTalkZone s)    = putRTy 39 s

-- 40: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.1)
putAttribute (AttrAcctStatusType s)    = putRTy 40 s


-- 41: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.2)
putAttribute (AttrAcctDelayTime s) = putRTy 42 s

-- 42: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.3)
putAttribute (AttrAcctInputOctets s) = putRTy 42 s

-- 43: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.4)
putAttribute (AttrAcctOutputOctets s) = putRTy 43 s

-- 44: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.5)
putAttribute (AttrAcctSessionId s) = putRTy 44 s

-- 45: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.6)
putAttribute (AttrAcctAuthentic s) = putRTy 45 s

-- 46: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.7)
putAttribute (AttrAcctSessionTime s) = putRTy 46 s

-- 47: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.8)
putAttribute (AttrAcctInputPackets s) = putRTy 47 s

-- 48: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.9)
putAttribute (AttrOutputPackets s) = putRTy 48 s

-- 49: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.10)
putAttribute (AttrAcctTerminateCause s) = putRTy 49 s

-- 50: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.11)
putAttribute (AttrAcctMultiSessionId s) = putRTy 50 s

-- 51: [RFC2866](https://tools.ietf.org/html/rfc2866#section-5.12)
putAttribute (AttrAcctLinkCount s) = putRTy 51 s

-- 52: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.1)
putAttribute (AttrAcctInputGigawords s) = putRTy 52 s

-- 53: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.2)
putAttribute (AttrAcctOutputGigawords s) = putRTy 53 s

-- 54: Unassigned

-- 55: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.3)
putAttribute (AttrEventTimestamp s) = putRTy 55 s

-- 56-59: RFC4675

-- 60: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.40)
putAttribute (AttrCHAPChallenge s) = putRTy 60 s

-- 61: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.41)
putAttribute (AttrNASPortType s) = putRTy 61 s

-- 62: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.42)
putAttribute (AttrPortLimit s) = putRTy 62 s

-- 63: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.43)
putAttribute (AttrLoginLATPort s) = putRTy 63 s

-- 64: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.1)
putAttribute (AttrTunnelType s) = putRTy 64 s

-- 65: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.2)
putAttribute (AttrTunnelMediumType s) = putRTy 65 s

-- 66: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.3)
putAttribute (AttrTunnelClientEndpoint Nothing s) = putRTy 66 s
putAttribute (AttrTunnelClientEndpoint (Just t) s) = do
    putWord8 t
    let buf = T.encodeUtf8 s
    putLength (fromIntegral $ SBS.length buf + 1)
    putByteString buf

-- 67: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.4)
putAttribute (AttrTunnelServerEndpoint Nothing s) = putRTy 66 s
putAttribute (AttrTunnelServerEndpoint (Just t) s) = do
    putWord8 t
    let buf = T.encodeUtf8 s
    putLength (fromIntegral $ SBS.length buf + 1)
    putByteString buf

-- 68: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.5)
putAttribute (AttrTunnelPassword s p) = do
  putLength (fromIntegral (S.length p))
  putWord16be s
  putByteString p

-- 69: Defined but not implemented.

-- 70: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.4)
putAttribute (AttrARAPPassword s) = putRTy 70 s

-- 71: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.5)
putAttribute (AttrARAPFeatures s) = putRTy 71 s

-- 72: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.6)
putAttribute (AttrARAPZoneAccess s) = putRTy 72 s

-- 73: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.7)
putAttribute (AttrARAPSecurity s) = putRTy 73 s

-- 74: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.8)
putAttribute (AttrARAPSecurityData s) = putRTy 74 s

-- 75: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.9)
putAttribute (AttrPasswordRetry s) = putRTy 75 s

-- 76: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.10)
putAttribute (AttrPrompt s) = putRTy 76 s

-- 77: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.11)
putAttribute (AttrConnectInfo s) = putRTy 77 s

-- 78: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.12)
putAttribute (AttrConfigurationToken s) = putRTy 78 s

-- 79: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.13)
putAttribute (AttrEAPMessage s) = putRTy 79 s

-- 80: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.14)
putAttribute (AttrMessageAuthenticator s) = putRTy 80 s

-- 81: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
putAttribute (AttrTunnelPrivateGroupId s) = putRTy 81 s

-- 82: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
putAttribute (AttrTunnelAssignmentId s) = putRTy 82 s

-- 83: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
putAttribute (AttrTunnelPreference s) = putRTy 83 s

-- 84: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.8)
putAttribute (AttrARAPChallengeResponse s) = putRTy 84 s

-- 85: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.16)
putAttribute (AttrAcctInterimInterval s) = putRTy 85 s

-- 86: RFC2867

-- 87: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.17)
putAttribute (AttrNASPortId s) = putRTy 87 s

-- 88: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.18)
putAttribute (AttrFramedPool s) = putRTy 88 s

-- 93: unassigned

-- 94: RFC7155

-- 95: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.1)
putAttribute (AttrNASIPv6Address s) = putRTy 95 s

-- 96: [RFC3162](https://tools.ietf.org/html/rfc3162#section-3.2)
putAttribute (AttrFramedInterfaceId s) = putRTy 96 s

-- 97: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.3)
putAttribute (AttrFramedIPv6Prefix r) = putRTy 97 r

-- 98: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.4)
putAttribute (AttrLoginIPv6Host s) = putRTy 98 s

-- 99: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.5)
putAttribute (AttrFramedIPv6Route s) = putRTy 99 s

-- 100: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.6)
putAttribute (AttrFramedIPv6Pool s) = putRTy 100 s

-- 123: [RFC4818](https://tools.ietf.org/html/rfc4818#section-3)
putAttribute (AttrDelegatedIPv6Prefix r) = putRTy 123 r

putAttribute (AttrUnknown ty str) = putWord8 ty >> putR str

getAttrComp :: Int -> Word8 -> Get Attribute

-- | 1: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.1)
getAttrComp l 1 = getR l <&> AttrUserName

-- | 2: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.2)
getAttrComp l 2 = getR l <&> AttrUserPassword

-- | 3: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.3)
getAttrComp l 3 = do
  c <- getWord8
  p <- getByteString (l - 1)

  pure (AttrCHAPPassword c p)

-- | 4: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.4)
getAttrComp l 4 = getR l <&> AttrNASIPAddress

-- | 5: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.5)
getAttrComp l 5 = getR l <&> AttrNASPort

-- | 6: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.6)
getAttrComp l 6 = getR l <&> AttrServiceType

-- | 7: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.7)
getAttrComp l 7 = getR l <&> AttrFramedProtocol

-- | 8: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.8)
getAttrComp l 8 = getR l <&> AttrFramedIPAddress

-- | 9: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.9)
getAttrComp l 9 = getR l <&> AttrFramedIPNetmask

-- | 10: [rfc2865](https://tools.ietf.org/html/rfc2865#section-5.10)
getAttrComp l 10 = getR l <&> AttrFramedRouting

-- | 11: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.11)
getAttrComp l 11 = getR l <&> AttrFilterId

-- | 12: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.12)
getAttrComp l 12 = getR l <&> AttrFramedMTU

-- | 13: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.13)
getAttrComp l 13 = getR l <&> AttrFramedCompression

-- 14: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.14)
getAttrComp l 14 = getR l <&> AttrLoginIPHost

-- 15: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.15)
getAttrComp l 15 = getR l <&> AttrLoginService

-- 16: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.16)
getAttrComp l 16 = getR l <&> AttrLoginTCPPort

-- unassigned!

-- 18: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.18)
getAttrComp l 18 = getR l <&> AttrReplyMessage

-- 19: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.19)
getAttrComp l 19 = getR l <&> AttrCallbackNumber

-- 20: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.20)
getAttrComp l 20 = getR l <&> AttrCallbackId

-- unassigned

-- 22: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.22)
getAttrComp l 22 = getR l <&> AttrFramedRoute

-- 23: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.23)
getAttrComp l 23 = getR l <&> AttrFramedIPXNetwork

-- 24: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.24)
getAttrComp l 24 = getR l <&> AttrState

-- 25: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.25)
getAttrComp l 25 = getR l <&> AttrClass

-- 26: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.26)
getAttrComp l 26 = do
  vendorId <- getWord32be

  case vendorId of
    9 -> do
      _vendorTy <- getWord8
      vendorLen <- getLength
      vendorStr <- getByteString vendorLen
      pure (AttrVendorSpecific (VSACisco vendorStr))
    _ -> do { vendorStr <- getByteString (l - 4) -- Take account of the vendor id
            ; pure (AttrVendorSpecific (VSAUnknown vendorId vendorStr))
            }

-- 27: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.27)
getAttrComp l 27 = getR l <&> AttrSessionTimeout

-- 28: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.28)
getAttrComp l 28 = getR l <&> AttrIdleTimeout

-- 29: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.29)
getAttrComp l 29 = getR l <&> AttrTerminationAction

-- 30: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.30)
getAttrComp l 30 = getR l <&> AttrCalledStationId

-- 31: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.31)
getAttrComp l 31 = getR l <&> AttrCallingStationId

-- 32: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.32)
getAttrComp l 32 = getR l <&> AttrNASIdentifier

-- 33: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.33)
getAttrComp l 33 = getR l <&> AttrProxyState

-- 34: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.34)
getAttrComp l 34 = getR l <&> AttrLoginLATService

-- 35: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.35)
getAttrComp l 35 = getR l <&> AttrLoginLATNode

-- 36: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.36)
getAttrComp l 36 = getR l <&> AttrLoginLATGroup

-- 37: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.37)
getAttrComp l 37 = getR l <&> AttrFramedAppleTalkLink

-- 38: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.38)
getAttrComp l 38 = getR l <&> AttrFramedAppleTalkNetwork

-- 39: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.39)
getAttrComp l 39 = getR l <&> AttrFramedAppleTalkZone

-- 40: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.1)
getAttrComp l 40 = getR l <&> AttrAcctStatusType

-- 41: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.2)
getAttrComp l 41 = getR l <&> AttrAcctDelayTime

-- 42: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.3)
getAttrComp l 42 = getR l <&> AttrAcctInputOctets

-- 43: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.4)
getAttrComp l 43 = getR l <&> AttrAcctOutputOctets

-- 44: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.5)
getAttrComp l 44 = getR l <&> AttrAcctSessionId

-- 45: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.6)
getAttrComp l 45 = getR l <&> AttrAcctAuthentic

-- 46: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.7)
getAttrComp l 46 = getR l <&> AttrAcctSessionTime

-- 47: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.8)
getAttrComp l 47 = getR l <&> AttrAcctInputPackets

-- 48: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.9)
getAttrComp l 48 = getR l <&> AttrOutputPackets

-- 49: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.10)
getAttrComp l 49 = getR l <&> AttrAcctTerminateCause

-- 50: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.11)
getAttrComp l 50 = getR l <&> AttrAcctMultiSessionId

-- 51: [RFC2866](https://tools.ietf.org/html/rfc2866#section-5.12)
getAttrComp l 51 = getR l <&> AttrAcctLinkCount

-- 52: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.1)
getAttrComp l 52 = getR l <&> AttrAcctInputGigawords

-- 53: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.2)
getAttrComp l 53 = getR l <&> AttrAcctOutputGigawords

-- 54: Unassigned

-- 55: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.3)
getAttrComp l 55 = getR l <&> AttrEventTimestamp

  -- 56-59: RFC4675

-- 60: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.40)
getAttrComp l 60 = getR l <&> AttrCHAPChallenge

-- 61: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.41)
getAttrComp l 61 = getR l <&> AttrNASPortType

-- 62: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.42)
getAttrComp l 62 = getR l <&> AttrPortLimit

-- 63: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.43)
getAttrComp l 63 = getR l <&> AttrLoginLATPort

-- 64: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.1)
getAttrComp l 64 = getR l <&> AttrTunnelType

-- 65: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.2)
getAttrComp l 65 = getR l <&> AttrTunnelMediumType

-- 66: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.3)
getAttrComp l 66 = do
    let l' = l - 1
    w <- getWord8
    if w > 31
      then do buf <- getByteString l'
              txt <- dec (w `S.cons` buf)
              pure (AttrTunnelClientEndpoint Nothing txt)
      else do buf <- getByteString l'
              txt <- dec buf
              pure (AttrTunnelClientEndpoint (Just w) txt)
  where
    dec b = case T.decodeUtf8' b of
        Left err -> fail ("Failed to decode UTF-8: " <> show err)
        Right t  -> pure t

-- 67: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.4)
getAttrComp l 67 = do
    let l' = l - 1
    w <- getWord8
    if w > 31
      then do buf <- getByteString l'
              txt <- dec (w `S.cons` buf)
              pure (AttrTunnelServerEndpoint Nothing txt)
      else do buf <- getByteString l'
              txt <- dec buf
              pure (AttrTunnelServerEndpoint (Just w) txt)
  where
    dec b = case T.decodeUtf8' b of
        Left err -> fail ("Failed to decode UTF-8: " <> show err)
        Right t  -> pure t

getAttrComp l 68 = AttrTunnelPassword <$> getWord16be <*> getR (l - 2)

-- 69: Defined but not implemented.

-- 70: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.4)
getAttrComp l 70 = getR l <&> AttrARAPPassword

-- 71: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.5)
getAttrComp l 71 = getR l <&> AttrARAPFeatures

-- 72: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.6)
getAttrComp l 72 = getR l <&> AttrARAPZoneAccess

-- 73: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.7)
getAttrComp l 73 = getR l <&> AttrARAPSecurity

-- 74: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.8)
getAttrComp l 74 = getR l <&> AttrARAPSecurityData

-- 75: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.9)
getAttrComp l 75 = getR l <&> AttrPasswordRetry

-- 76: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.10)
getAttrComp l 76 = getR l <&> AttrPrompt

-- 77: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.11)
getAttrComp l 77 = getR l <&> AttrConnectInfo

-- 78: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.12)
getAttrComp l 78 = getR l <&> AttrConfigurationToken

-- 79: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.13)
getAttrComp l 79 = getR l <&> AttrEAPMessage

-- 80: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.14)
getAttrComp l 80 = getR l <&> AttrMessageAuthenticator

-- 81: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
getAttrComp l 81 = getR l <&> AttrTunnelPrivateGroupId

-- 82: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
getAttrComp l 82 = getR l <&> AttrTunnelAssignmentId

-- 83: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
getAttrComp l 83 = getR l <&> AttrTunnelPreference

-- 84: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.8)
getAttrComp l 84 = getR l <&> AttrARAPChallengeResponse

-- 85: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.16)
getAttrComp l 85 = getR l <&> AttrAcctInterimInterval

-- 86: RFC2867

-- 87: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.17)
getAttrComp l 87 = getR l <&> AttrNASPortId

-- 88: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.18)
getAttrComp l 88 = getR l <&> AttrFramedPool

-- 93: unassigned

-- 94: RFC7155

-- 95: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.1)
getAttrComp l 95 = getR l <&> AttrNASIPv6Address

-- 96: [RFC3162](https://tools.ietf.org/html/rfc3162#section-3.2)
getAttrComp l 96 = getR l <&> AttrFramedInterfaceId

-- 97: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.3)
getAttrComp l 97 = getR l <&> AttrFramedIPv6Prefix

-- 98: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.4)
getAttrComp l 98 = getR l <&> AttrLoginIPv6Host

-- 99: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.5)
getAttrComp l 99 = getR l <&> AttrFramedIPv6Route

-- 100: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.6)
getAttrComp l 100 = getR l <&> AttrFramedIPv6Pool

-- 123: [RFC4818](https://tools.ietf.org/html/rfc4818#section-3)
getAttrComp l 123 = getR l <&> AttrDelegatedIPv6Prefix

getAttrComp l t =  getR l <&> (AttrUnknown t)

(<&>) :: (Functor f) => f a -> (a -> b) -> f b
(<&>) = flip fmap

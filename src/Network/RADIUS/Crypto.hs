{-# LANGUAGE OverloadedStrings #-}
module Network.RADIUS.Crypto
  ( sign
  , encodePassword
  , decodePassword
  )

where

import           Crypto.Data.Padding (Format(..), pad)
import           Crypto.Hash (hashWith)
import           Crypto.Hash.Algorithms (MD5(MD5))
import           Data.Bits (xor)
import qualified Data.ByteArray as B (convert)
import qualified Data.ByteString as SBS (ByteString, drop, length, pack,
                                         spanEnd, take, zipWith)
import qualified Data.ByteString.Unsafe as U (unsafeDrop, unsafeTake)

import           Data.ByteArray (convert)

-- | Signs an already serialized packet with an authenticator.
sign :: SBS.ByteString -> SBS.ByteString -> SBS.ByteString
sign packet secret =
    let authenticator = hashMD5 (packet <> secret)
        prologue      = SBS.take 4 packet
        attributes    = SBS.drop 20 packet
    in prologue <> authenticator <> attributes

hashMD5 :: SBS.ByteString -> SBS.ByteString
hashMD5 = convert . hashWith MD5

encodePasswordSalt :: SBS.ByteString       -- ^ Authenticator
                   -> SBS.ByteString       -- ^ Salt
                   -> SBS.ByteString       -- ^ Secret
                   -> SBS.ByteString       -- ^ Plain password
                   -> SBS.ByteString       -- ^ Encoded password
encodePasswordSalt auth salt secret pass = encodePassword (auth <> salt) secret pass

encodePassword :: SBS.ByteString       -- ^ Authenticator
               -> SBS.ByteString       -- ^ Secret
               -> SBS.ByteString       -- ^ Plain password
               -> SBS.ByteString       -- ^ Encoded password
encodePassword  auth secret pass = go auth padded
  where
    padded = pad (ZERO 16) pass
    len    = SBS.length padded
    go lastRound restPass
        | restPass == ""
        = mempty
        | otherwise
        = chunk <> go chunk rest
      where
        chunk         = SBS.pack (SBS.zipWith xor bytes hash)
        (bytes, rest) = (U.unsafeTake 16 restPass, U.unsafeDrop 16 restPass)
        hash          = B.convert (hashWith MD5 (secret <> lastRound))

decodePasswordSalt :: SBS.ByteString       -- ^ Authenticator
                   -> SBS.ByteString       -- ^ Salt
                   -> SBS.ByteString       -- ^ Secret
                   -> SBS.ByteString       -- ^ Encoded password
                   -> Maybe SBS.ByteString -- ^ Plain password
decodePasswordSalt auth salt secret pass = decodePassword (auth <> salt) secret pass

decodePassword :: SBS.ByteString       -- ^ Authenticator
               -> SBS.ByteString       -- ^ Secret
               -> SBS.ByteString       -- ^ Encoded password
               -> Maybe SBS.ByteString -- ^ Plain password
decodePassword auth secret pass
  | len `mod` 16 == 0 = Just (unpad (go auth pass))
  | otherwise         = Nothing
  where
    len = SBS.length pass
    unpad = fst . SBS.spanEnd (==0)
    go lastRound restPass
        | restPass == ""
        = mempty
        | otherwise
        = chunk <> go bytes rest
      where
        chunk         = SBS.pack (SBS.zipWith xor bytes hash)
        (bytes, rest) = (U.unsafeTake 16 restPass, U.unsafeDrop 16 restPass)
        hash          = B.convert (hashWith MD5 (secret <> lastRound))

{-|
Module      : Network.RADIUS.Types
Description : Provides types and definitions for RADIUS as per RFC 2865
Copyright   : (c) Victor Nawothnig, 2018
License     : BSD3
Maintainer  : Victor.Nawothnig@gmail.com
Stability   : experimental
Portability : POSIX

This module compiles the RADIUS packet definitions and different attributes as specified in
RFC 2865. The naming conventions from the RFC have been preserved as much as possible, so
it should be straightforward to look up a particular element and understand what it means etc.

RADIUS extensions in RFC 2869 are also supported, as well as RFC 3162 for IPv6 related attributes
-}

module Network.RADIUS.Types
  ( NumSerializable(..)
  , Packet(..)
  , PacketType(..)
  , VSA(..)
  , Attribute(..)
  , IngressFiltersState(..)
  , AcctStatusType(..)
  , Authentic(..)
  , TerminateCause(..)
  , ServiceType(..)
  , FramedProtocol(..)
  , FramedRouting(..)
  , FramedCompression(..)
  , LoginService(..)
  , TerminationAction(..)
  , NASPortType(..)
  , ARAPZoneAccess(..)
  , TunnelType(..)
  , TunnelMediumType(..)
  )
where

import Data.Text (Text)
import Data.Word (Word16, Word32, Word64, Word8)

import Control.Monad.Fail (MonadFail)
import Data.ByteString.Char8 (ByteString)
import Data.IP
import Data.Semigroup ((<>))

data Packet = Packet { packetType          :: !PacketType
                     , packetId            :: !Word8
                     , packetLength        :: !Word16
                     , packetAuthenticator :: !ByteString
                     , packetAttributes    :: ![Attribute]
                     } deriving (Show, Eq)

class NumSerializable a where
  serialize :: (Integral n) => a -> n
  deserialize :: (MonadFail f, Integral n, Show n) => n -> f a

data RadiusType =
    Access
  | Accounting
  deriving (Eq, Show)

data PacketType =
  -- | 1: [RFC2865](https://tools.ietf.org/html/rfc2865#section-4.1)
    AccessRequest

  -- | 2: [RFC2865](https://tools.ietf.org/html/rfc2865#section-4.2)
  | AccessAccept

  -- | 3: [RFC2865](https://tools.ietf.org/html/rfc2865#section-4.3)
  | AccessReject

  -- | 4: [RFC2866](https://tools.ietf.org/html/rfc2866#section-4.1)
  | AccountingRequest

  -- | 5: [RFC2866](https://tools.ietf.org/html/rfc2866#section-4.2)
  | AccountingResponse

  -- | 11: [RFC2865](https://tools.ietf.org/html/rfc2865#section-4.4)
  | AccessChallenge

  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable PacketType where
  serialize p = case p of
    AccessRequest      -> 1
    AccessAccept       -> 2
    AccessReject       -> 3
    AccountingRequest  -> 4
    AccountingResponse -> 5
    AccessChallenge    -> 11

  deserialize p = case p of
    1  -> pure AccessRequest
    2  -> pure AccessAccept
    3  -> pure AccessReject
    4  -> pure AccountingRequest
    5  -> pure AccountingResponse
    11 -> pure AccessChallenge
    _  -> fail ("Unknown RADIUS Packet-Type: " <> show p)

data Attribute =
    AttrUnknown           !Word8 !ByteString

  -- | 1: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.1)
  | AttrUserName          !Text

  -- | 2: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.2)
  | AttrUserPassword      !ByteString

  -- | 3: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.3)
  | AttrCHAPPassword      !Word8 !ByteString

  -- | 4: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.4)
  | AttrNASIPAddress      !IPv4

  -- | 5: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.5)
  | AttrNASPort           !Word32

  -- | 6: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.6)
  | AttrServiceType       !ServiceType

  -- | 7: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.7)
  | AttrFramedProtocol    !FramedProtocol

  -- | 8: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.8)
  | AttrFramedIPAddress   !IPv4

  -- | 9: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.9)
  | AttrFramedIPNetmask   !IPv4

  -- | 10: [rfc2865](https://tools.ietf.org/html/rfc2865#section-5.10)
  | AttrFramedRouting     !FramedRouting

  -- | 11: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.11)
  | AttrFilterId          !Text

  -- | 12: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.12)
  | AttrFramedMTU         !Word32

  -- | 13: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.13)
  | AttrFramedCompression !FramedCompression

  -- | 14: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.14)
  | AttrLoginIPHost       !IPv4

  -- | 15: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.15)
  | AttrLoginService      !LoginService

  -- | 16: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.16)
  | AttrLoginTCPPort      !Word32

  -- unassigned!

  -- | 18: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.18)
  | AttrReplyMessage      !Text

  -- | 19: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.19)
  | AttrCallbackNumber    !Text

  -- | 20: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.20)
  | AttrCallbackId        !Text

  -- unassigned

  -- | 22: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.22)
  | AttrFramedRoute       !Text

  -- | 23: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.23)
  | AttrFramedIPXNetwork  !Word32

  -- | 24: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.24)
  | AttrState             !ByteString

  -- | 25: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.25)
  | AttrClass             !ByteString

  -- | 26: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.26)
  | AttrVendorSpecific    !VSA

  -- | 27: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.27)
  | AttrSessionTimeout    !Word32

  -- | 28: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.28)
  | AttrIdleTimeout       !Word32

  -- | 29: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.29)
  | AttrTerminationAction !TerminationAction

  -- | 30: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.30)
  | AttrCalledStationId   !Text

  -- | 31: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.31)
  | AttrCallingStationId  !Text

  -- | 32: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.32)
  | AttrNASIdentifier     !Text

  -- | 33: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.33)
  | AttrProxyState        !ByteString

  -- | 34: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.34)
  | AttrLoginLATService   !Text

  -- | 35: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.35)
  | AttrLoginLATNode      !Text

  -- | 36: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.36)
  | AttrLoginLATGroup     !ByteString

  -- | 37: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.37)
  | AttrFramedAppleTalkLink    !Word32

  -- | 38: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.38)
  | AttrFramedAppleTalkNetwork !Word32

  -- | 39: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.39)
  | AttrFramedAppleTalkZone    !Text

  -- | 40: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.1)
  | AttrAcctStatusType !AcctStatusType

  -- | 41: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.2)
  | AttrAcctDelayTime  !Word32

  -- | 42: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.3)
  | AttrAcctInputOctets !Word32

  -- | 43: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.4)
  | AttrAcctOutputOctets !Word32

  -- | 44: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.5)
  | AttrAcctSessionId !Text

  -- | 45: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.6)
  | AttrAcctAuthentic !Authentic

  -- | 46: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.7)
  | AttrAcctSessionTime !Word32

  -- | 47: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.8)
  | AttrAcctInputPackets !Word32

  -- | 48: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.9)
  | AttrOutputPackets !Word32

  -- | 49: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.10)
  | AttrAcctTerminateCause !TerminateCause

  -- | 50: [RFC2869](https://tools.ietf.org/html/rfc2866#section-5.11)
  | AttrAcctMultiSessionId !Text

  -- | 51: [RFC2866](https://tools.ietf.org/html/rfc2866#section-5.12)
  | AttrAcctLinkCount !Word32

  -- | 52: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.1)
  | AttrAcctInputGigawords !Word32

  -- | 53: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.2)
  | AttrAcctOutputGigawords !Word32

  -- 54: Unassigned

  -- | 55: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.3)
  | AttrEventTimestamp !Word32

    -- 56-59: RFC4675

  -- | 60: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.40)
  | AttrCHAPChallenge !ByteString

  -- | 61: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.41)
  | AttrNASPortType       !NASPortType

  -- | 62: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.42)
  | AttrPortLimit         !Word32

  -- | 63: [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.43)
  | AttrLoginLATPort      !Text

  -- | 64: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.1)
  | AttrTunnelType        !TunnelType

  -- | 65: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.2)
  | AttrTunnelMediumType  !TunnelMediumType

  -- | 66: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.3)
  | AttrTunnelClientEndpoint !(Maybe Word8) !Text

  -- | 67: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.4)
  | AttrTunnelServerEndpoint !(Maybe Word8) !Text

  -- 68 Undefined

  -- | 69: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.5)
  | AttrTunnelPassword    !Word16 !ByteString

  -- | 70: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.4)
  | AttrARAPPassword      !ByteString

  -- | 71: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.5)
  | AttrARAPFeatures      !ByteString

  -- | 72: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.6)
  | AttrARAPZoneAccess    !ARAPZoneAccess

  -- | 73: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.7)
  | AttrARAPSecurity      !Word32

  -- | 74: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.8)
  | AttrARAPSecurityData  !Text

  -- | 75: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.9)
  | AttrPasswordRetry     !Word32

  -- | 76: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.10)
  | AttrPrompt            !Word32

  -- | 77: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.11)
  | AttrConnectInfo       !Text

  -- | 78: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.12)
  | AttrConfigurationToken     !Text

  -- | 79: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.13)
  | AttrEAPMessage             !ByteString

  -- | 80: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.14)
  | AttrMessageAuthenticator   !ByteString

  -- 81-83: RFC2868

  -- | 81: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
  | AttrTunnelPrivateGroupId   !Text

  -- | 82: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
  | AttrTunnelAssignmentId     !Text

  -- | 83: [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.6)
  | AttrTunnelPreference       !Word32

  -- | 84: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.8)
  | AttrARAPChallengeResponse  !ByteString

  -- | 85: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.16)
  | AttrAcctInterimInterval    !Word32

  -- 86: RFC2867

  -- | 87: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.17)
  | AttrNASPortId         !Text

  -- | 88: [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.18)
  | AttrFramedPool        !Text

  -- 93: unassigned

  -- 94: RFC7155

  -- | 95: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.1)
  | AttrNASIPv6Address    !IPv6

  -- | 96: [RFC3162](https://tools.ietf.org/html/rfc3162#section-3.2)
  | AttrFramedInterfaceId !Word64

  -- | 97: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.3)
  | AttrFramedIPv6Prefix  !(AddrRange IPv6)

  -- | 98: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.4)
  | AttrLoginIPv6Host     !IPv6

  -- | 99: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.5)
  | AttrFramedIPv6Route   !Text

  -- | 100: [RFC3162](https://tools.ietf.org/html/rfc3162#section-2.6)
  | AttrFramedIPv6Pool    !Text

  -- | 123: [RFC4818](https://tools.ietf.org/html/rfc4818#section-3)
  | AttrDelegatedIPv6Prefix !(AddrRange IPv6)
  deriving (Show, Eq)


data VSA
  = VSAUnknown !Word32 !ByteString
  | VSACisco !ByteString
  deriving (Show, Eq)

data IngressFiltersState
  = IngressFiltersDisabled
  | IngressFiltersEnabled
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable IngressFiltersState where
  serialize i = case i of
    IngressFiltersDisabled -> 1
    IngressFiltersEnabled  -> 2
  deserialize s = case s of
    1 -> pure IngressFiltersDisabled
    2 -> pure IngressFiltersEnabled
    _ -> fail ("Unknown RADIUS Ingress-Filters-State: " <> show s)


-- | [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.1)
data TunnelType
  = TT_PPTP
  | TT_L2F
  | TT_L2TP
  | TT_ATMP
  | TT_VTP
  | TT_AH
  | TT_IP_IP
  | TT_MIN_IP_IP
  | TT_ESP
  | TT_GRE
  | TT_DVS
  | TT_IP_IP_TUN
  | TT_VLAN -- [RFC3580](https://tools.ietf.org/html/rfc3580#section-3.31)
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable TunnelType where
  serialize i = case i of
    TT_PPTP      -> 1
    TT_L2F       -> 2
    TT_L2TP      -> 3
    TT_ATMP      -> 4
    TT_VTP       -> 5
    TT_AH        -> 6
    TT_IP_IP     -> 7
    TT_MIN_IP_IP -> 8
    TT_ESP       -> 9
    TT_GRE       -> 10
    TT_DVS       -> 11
    TT_IP_IP_TUN -> 12
    TT_VLAN      -> 13
  deserialize s = case s of
    1  -> pure TT_PPTP
    2  -> pure TT_L2F
    3  -> pure TT_L2TP
    4  -> pure TT_ATMP
    5  -> pure TT_VTP
    6  -> pure TT_AH
    7  -> pure TT_IP_IP
    8  -> pure TT_MIN_IP_IP
    9  -> pure TT_ESP
    10 -> pure TT_GRE
    11 -> pure TT_DVS
    12 -> pure TT_IP_IP_TUN
    13 -> pure TT_VLAN
    _  -> fail ("Unknown RADIUS Tunnel-Type: " <> show s)


-- | [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.2)
data TunnelMediumType
  = TMT_IPv4
  | TMT_IPv6
  | TMT_NSAP
  | TMT_HDLC
  | TMT_BBN1822
  | TMT_802
  | TMT_E163
  | TMT_E164
  | TMT_F69
  | TMT_X121
  | TMT_IPX
  | TMT_AppleTalk
  | TMT_Decnet_IV
  | TMT_Banyan_Vines
  | TMT_E164_NSAP
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable TunnelMediumType where
  serialize i = case i of
    TMT_IPv4         -> 1
    TMT_IPv6         -> 2
    TMT_NSAP         -> 3
    TMT_HDLC         -> 4
    TMT_BBN1822      -> 5
    TMT_802          -> 6
    TMT_E163         -> 7
    TMT_E164         -> 8
    TMT_F69          -> 9
    TMT_X121         -> 10
    TMT_IPX          -> 11
    TMT_AppleTalk    ->  12
    TMT_Decnet_IV    -> 13
    TMT_Banyan_Vines -> 14
    TMT_E164_NSAP    -> 15
  deserialize s = case s of
    1  -> pure TMT_IPv4
    2  -> pure TMT_IPv6
    3  -> pure TMT_NSAP
    4  -> pure TMT_HDLC
    5  -> pure TMT_BBN1822
    6  -> pure TMT_802
    7  -> pure TMT_E163
    8  -> pure TMT_E164
    9  -> pure TMT_F69
    10 -> pure TMT_X121
    11 -> pure TMT_IPX
    12 -> pure TMT_AppleTalk
    13 -> pure TMT_Decnet_IV
    14 -> pure TMT_Banyan_Vines
    15 -> pure TMT_E164_NSAP
    _  -> fail ("Unknown RADIUS Tunnel-Medium-Type: " <> show s)


-- | [RFC2866](https://tools.ietf.org/html/rfc2866#section-5.1)
data AcctStatusType
  = AcctStart
  | AcctStop
  | AcctInterimUpdate
  | AcctAccountingOn
  | AcctAccountingOff
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable AcctStatusType where
  serialize s = case s of
    AcctStart         -> 1
    AcctStop          -> 2
    AcctInterimUpdate -> 3
    AcctAccountingOn  -> 7
    AcctAccountingOff -> 8
  deserialize s = case s of
    1 -> pure AcctStart
    2 -> pure AcctStop
    3 -> pure AcctInterimUpdate
    7 -> pure AcctAccountingOn
    8 -> pure AcctAccountingOff
    _ -> fail ("Unknown RADIUS Acct-Status-Type: " <> show s)

-- | [RFC2866](https://tools.ietf.org/html/rfc2866#section-5.6)
data Authentic
  = AuthenticRADIUS
  | AuthenticLocal
  | AuthenticRemote
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable Authentic where
  serialize s = case s of
    AuthenticRADIUS -> 1
    AuthenticLocal  -> 2
    AuthenticRemote -> 3
  deserialize s = case s of
    1 -> pure AuthenticRADIUS
    2 -> pure AuthenticLocal
    3 -> pure AuthenticRemote
    _ -> fail ("Unknown RADIUS Authentic: " <> show s)

-- | [RFC2866](https://tools.ietf.org/html/rfc2866#section-5.10)
data TerminateCause
  = TcUserRequest
  | TcLostCarrier
  | TcLostService
  | TcIdleTimeout
  | TcSessionTimeout
  | TcAdminReset
  | TcAdminReboot
  | TcPortError
  | TcNASError
  | TcNASRequest
  | TcNASReboot
  | TcPortUnneeded
  | TcPortPreempted
  | TcPortSuspended
  | TcServiceUnavailable
  | TcCallback
  | TcUserError
  | TcHostRequest
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable TerminateCause where
  serialize tc = case tc of
    TcUserRequest        -> 1
    TcLostCarrier        -> 2
    TcLostService        -> 3
    TcIdleTimeout        -> 4
    TcSessionTimeout     -> 5
    TcAdminReset         -> 6
    TcAdminReboot        -> 7
    TcPortError          -> 8
    TcNASError           -> 9
    TcNASRequest         -> 10
    TcNASReboot          -> 11
    TcPortUnneeded       -> 12
    TcPortPreempted      -> 13
    TcPortSuspended      -> 14
    TcServiceUnavailable -> 15
    TcCallback           -> 16
    TcUserError          -> 17
    TcHostRequest        -> 18
  deserialize s = case s of
    1  -> pure TcUserRequest
    2  -> pure TcLostCarrier
    3  -> pure TcLostService
    4  -> pure TcIdleTimeout
    5  -> pure TcSessionTimeout
    6  -> pure TcAdminReset
    7  -> pure TcAdminReboot
    8  -> pure TcPortError
    9  -> pure TcNASError
    10 -> pure TcNASRequest
    11 -> pure TcNASReboot
    12 -> pure TcPortUnneeded
    13 -> pure TcPortPreempted
    14 -> pure TcPortSuspended
    15 -> pure TcServiceUnavailable
    16 -> pure TcCallback
    17 -> pure TcUserError
    18 -> pure TcHostRequest
    _  -> fail ("Unknown RADIUS Terminate-Cause: " <> show s)


-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.6)
data ServiceType
  = StLogin
  | StFramed
  | StCallbackLogin
  | StCallbackFramed
  | StOutbound
  | StAdministrative
  | StNASPrompt
  | StAuthenticateOnly
  | StCallbackNASPrompt
  | StCallCheck
  | StCallbackAdministrative
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable ServiceType where
  serialize s = case s of
    StLogin                  -> 1
    StFramed                 -> 2
    StCallbackLogin          -> 3
    StCallbackFramed         -> 4
    StOutbound               -> 5
    StAdministrative         -> 6
    StNASPrompt              -> 7
    StAuthenticateOnly       -> 8
    StCallbackNASPrompt      -> 9
    StCallCheck              -> 10
    StCallbackAdministrative -> 11
  deserialize s = case s of
    1  -> pure StLogin
    2  -> pure StFramed
    3  -> pure StCallbackLogin
    4  -> pure StCallbackFramed
    5  -> pure StOutbound
    6  -> pure StAdministrative
    7  -> pure StNASPrompt
    8  -> pure StAuthenticateOnly
    9  -> pure StCallbackNASPrompt
    10 -> pure StCallCheck
    11 -> pure StCallbackAdministrative
    _  -> fail ("Unknown RADIUS Service-Type: " <> show s)

-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.7)
data FramedProtocol
  = FpPPP
  | FpSLIP
  | FpARAP
  | FpGandalf
  | FpXylogics
  | FpX75
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable FramedProtocol where
  serialize s = case s of
    FpPPP      -> 1
    FpSLIP     -> 2
    FpARAP     -> 3
    FpGandalf  -> 4
    FpXylogics -> 5
    FpX75      -> 6
  deserialize s  = case s of
    1 -> pure FpPPP
    2 -> pure FpSLIP
    3 -> pure FpARAP
    4 -> pure FpGandalf
    5 -> pure FpXylogics
    6 -> pure FpX75
    _ -> fail ("Unknown RADIUS Framed-Protocol: " <> show s)

-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.10)
data FramedRouting
  = FrNone
  | FrSend
  | FrListen
  | FrSendAndListen
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable FramedRouting where
  serialize s = case s of
    FrNone          -> 0
    FrSend          -> 1
    FrListen        -> 2
    FrSendAndListen -> 3
  deserialize s = case s of
    0 -> pure FrNone
    1 -> pure FrSend
    2 -> pure FrListen
    3 -> pure FrSendAndListen
    _ -> fail ("Unknown RADIUS Framed-Routing: " <> show s)

-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.13)
data FramedCompression
  = FcNone
  | FcVJTCPIPHeader
  | FcIPXHeader
  | FcStacLZS
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable FramedCompression where
  serialize s = case s of
    FcNone          -> 0
    FcVJTCPIPHeader -> 1
    FcIPXHeader     -> 2
    FcStacLZS       -> 3
  deserialize s = case s of
    0 -> pure FcNone
    1 -> pure FcVJTCPIPHeader
    2 -> pure FcIPXHeader
    3 -> pure FcStacLZS
    _ -> fail ("Unknown RADIUS Framed-Compression: " <> show s)

-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.15)
data LoginService
  = LsTelnet
  | LsRlogin
  | LsTCPClear
  | LsPortMaster
  | LsLAT
  | LsX25PAD
  | LsX25T3POS
  | LsTCPClearQuiet
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable LoginService where
  serialize ls = case ls of
    LsTelnet        -> 0
    LsRlogin        -> 1
    LsTCPClear      -> 2
    LsPortMaster    -> 3
    LsLAT           -> 4
    LsX25PAD        -> 5
    LsX25T3POS      -> 6
    -- 7 is not defined
    LsTCPClearQuiet -> 8

  deserialize ls = case ls of
    0 -> pure LsTelnet
    1 -> pure LsRlogin
    2 -> pure LsTCPClear
    3 -> pure LsPortMaster
    4 -> pure LsLAT
    5 -> pure LsX25PAD
    6 -> pure LsX25T3POS
    -- 7 is not defined
    8 -> pure LsTCPClearQuiet
    _ -> fail ("Unknown RADIUS Login-Service: " <> show ls)

-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.29)
data TerminationAction
  = TaDefault
  | TaRADIUSRequest
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable TerminationAction where
  serialize ta = case ta of
    TaDefault       -> 0
    TaRADIUSRequest -> 1
  deserialize ta = case ta of
    0 -> pure TaDefault
    1 -> pure TaRADIUSRequest
    _ -> fail ("Unknown RADIUS Termination-Action: " <> show ta)

-- | [RFC2865](https://tools.ietf.org/html/rfc2865#section-5.41)
data NASPortType
  = NptAsync
  | NptSync
  | NptISDNSync
  | NptISDNAsyncV120
  | NptISDNAsyncV110
  | NptVirtual
  | NptPIAFS
  | NptHDLCClearChannel
  | NptX25
  | NptX75
  | NptG3Fax
  | NptSDSL
  | NptADSLCAP
  | NptADSLDMT
  | NptIDSL
  | NptEthernet
  | NptXDSL
  | NptCable
  | NptWirelessOther
  | NptWirelessIEEE80211
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable NASPortType where
  serialize s = case s of
    NptAsync             -> 0
    NptSync              -> 1
    NptISDNSync          -> 2
    NptISDNAsyncV120     -> 3
    NptISDNAsyncV110     -> 4
    NptVirtual           -> 5
    NptPIAFS             -> 6
    NptHDLCClearChannel  -> 7
    NptX25               -> 8
    NptX75               -> 9
    NptG3Fax             -> 10
    NptSDSL              -> 11
    NptADSLCAP           -> 12
    NptADSLDMT           -> 13
    NptIDSL              -> 14
    NptEthernet          -> 15
    NptXDSL              -> 16
    NptCable             -> 17
    NptWirelessOther     -> 18
    NptWirelessIEEE80211 -> 19
  deserialize s = case s of
    0  -> pure NptAsync
    1  -> pure NptSync
    2  -> pure NptISDNSync
    3  -> pure NptISDNAsyncV120
    4  -> pure NptISDNAsyncV110
    5  -> pure NptVirtual
    6  -> pure NptPIAFS
    7  -> pure NptHDLCClearChannel
    8  -> pure NptX25
    9  -> pure NptX75
    10 -> pure NptG3Fax
    11 -> pure NptSDSL
    12 -> pure NptADSLCAP
    13 -> pure NptADSLDMT
    14 -> pure NptIDSL
    15 -> pure NptEthernet
    16 -> pure NptXDSL
    17 -> pure NptCable
    18 -> pure NptWirelessOther
    19 -> pure NptWirelessIEEE80211
    _  -> fail ("Unknown RADIUS NAS-Port-Type: " <> show s)

--------------------------------------------------------------------------------

-- | [RFC2869](https://tools.ietf.org/html/rfc2869#section-5.6)
data ARAPZoneAccess
  = DefaultZoneOnly
  | UseZoneFilterInclusively
  | UseZoneFilterExclusively
  deriving (Show, Eq, Bounded, Enum)

instance NumSerializable ARAPZoneAccess where
  serialize s = case s of
    DefaultZoneOnly          -> 1
    UseZoneFilterInclusively -> 2
    UseZoneFilterExclusively -> 4
  deserialize s = case s of
    1 -> pure DefaultZoneOnly
    2 -> pure UseZoneFilterInclusively
    4 -> pure UseZoneFilterExclusively
    _ -> fail ("Unknown RADIUS ARAP-Zone-Access: " <> show s)

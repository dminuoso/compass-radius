# Remote Authentication Dial In User Service (RADIUS)

compass-radius is an RFC-compliant RADIUS de-/serialization package.

It is a fork of [this package by codemonkeylabs](https://gitlab.com/codemonkeylabs/RADIUS) to include support for strict bytestrings, avoid error and include some additional
RADIUS extensions.

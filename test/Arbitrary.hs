module Arbitrary where

import qualified Data.ByteString as SBS
import qualified Data.ByteString.Char8 as CBS
import           Data.Typeable (Typeable)
import           Network.RADIUS.Types
import           Test.QuickCheck

instance Arbitrary PacketType where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary IngressFiltersState where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary AcctStatusType where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary Authentic where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary TerminateCause where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary ServiceType where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary FramedProtocol where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary FramedRouting where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary FramedCompression where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary LoginService  where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary TerminationAction where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary NASPortType where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary ARAPZoneAccess where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary TunnelType where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary TunnelMediumType where
  arbitrary = arbitraryBoundedEnum

arbitraryPrintableASCIIChar :: Gen Char
arbitraryPrintableASCIIChar = choose ('\33', '\126')

newtype PrintableASCIIString = PrintableASCIIString {getPrintableASCIIString :: String}
  deriving (Eq, Ord, Show, Read, Typeable)

instance Arbitrary PrintableASCIIString where
  arbitrary = PrintableASCIIString `fmap` listOf arbitraryPrintableASCIIChar
  shrink (PrintableASCIIString xs) = PrintableASCIIString `fmap` shrink xs

newtype PrintableASCIIByteString = PrintableASCIIByteString SBS.ByteString
  deriving (Eq, Show)

instance Arbitrary PrintableASCIIByteString where
  arbitrary = PrintableASCIIByteString . CBS.pack . getPrintableASCIIString <$> arbitrary

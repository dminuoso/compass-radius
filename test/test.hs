{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TypeApplications  #-}

import           Data.String
import           Hexdump

import           Test.Tasty
import           Test.Tasty.QuickCheck as QC

import           Data.List
import           Data.Ord

import           Data.Serialize (Serialize, get, put)
import           Data.Serialize.Get (runGet)
import           Data.Serialize.Put (runPut)

import qualified Data.ByteString as BS

import           Network.RADIUS.Crypto hiding (sign)
import           Network.RADIUS.Encoding
import           Network.RADIUS.Types

import           Arbitrary

import           Data.Hex.Quote (hex)

main = defaultMain tests

tests :: TestTree
tests = testGroup "RADIUS works!" [ props_encoding
                                  , props_crypto
                                  , props_rfc_examples
                                  ]

props_encoding :: TestTree
props_encoding = testGroup "RADIUS attributes are correctly de/encoded"
  [ getPut
  ]

getPutIsId :: (Serialize a, Eq a) => a -> Bool
getPutIsId a = (runGet get . runPut . put) a == Right a

getRPutRIsId :: (RadiusSer a, Eq a) => a -> Bool
getRPutRIsId a = (runGet getRL . runPut . putR) a == Right a

liftA0 :: Applicative f => a -> f a
liftA0 = pure

getPut :: TestTree
getPut = testGroup "get . put ~ pure"
  [ QC.testProperty "PacketType"          $ getPutIsId @PacketType
  , QC.testProperty "ARAPZoneAccess"      $ getRPutRIsId @ARAPZoneAccess
  , QC.testProperty "NASPortType"         $ getRPutRIsId @NASPortType
  , QC.testProperty "TerminationAction"   $ getRPutRIsId @TerminationAction
  , QC.testProperty "LoginService"        $ getRPutRIsId @LoginService
  , QC.testProperty "FramedCompression"   $ getRPutRIsId @FramedCompression
  , QC.testProperty "FramedRouting"       $ getRPutRIsId @FramedRouting
  , QC.testProperty "FramedProtocol"      $ getRPutRIsId @FramedProtocol
  , QC.testProperty "ServiceType"         $ getRPutRIsId @ServiceType
  , QC.testProperty "TerminateCause"      $ getRPutRIsId @TerminateCause
  , QC.testProperty "Authentic"           $ getRPutRIsId @Authentic
  , QC.testProperty "AcctStatusType"      $ getRPutRIsId @AcctStatusType
  , QC.testProperty "IngressFiltersState" $ getRPutRIsId @IngressFiltersState
  , QC.testProperty "TunnelType"          $ getRPutRIsId @TunnelType
  , QC.testProperty "TunnelMediumType"    $ getRPutRIsId @TunnelMediumType
  , QC.testProperty "VendorSpecific: Cisco" $ cisco_vsa_holds_identity
  ]

props_crypto :: TestTree
props_crypto = testGroup "Cryptographic modules work correctly"
  [ QC.testProperty "decode . encode ~ Just" encodeDecodeIsJust
  ]

slowRandBs :: Int -> Gen BS.ByteString
slowRandBs numBytes = BS.pack `fmap` vectorOf numBytes (choose (0, 255))

cisco_vsa_holds_identity :: Gen Bool
cisco_vsa_holds_identity = idHolds <$> genCiscoAttr
  where
    genCiscoAttr :: Gen Attribute
    genCiscoAttr = (AttrVendorSpecific . VSACisco) <$> oneof (slowRandBs <$> [0..251])

    idHolds :: Attribute -> Bool
    idHolds attr = transmuted == Right attr
      where
        transmuted = (runGet getAttribute . runPut . putAttribute) attr

encodeDecodeIsJust :: (PrintableASCIIByteString, PrintableASCIIByteString, PrintableASCIIByteString) -> Bool
encodeDecodeIsJust (PrintableASCIIByteString auth, PrintableASCIIByteString secret, PrintableASCIIByteString password) = Just password == transmute auth secret password
  where
    transmute auth secret = decodePassword auth secret . encodePassword auth secret


-- Radius examples from RFC2865 Section 7

props_rfc_examples :: TestTree
props_rfc_examples = testGroup "Examples according to RFC2865"
  [ testProperty "Example 7.1 Request" example71req
  , testProperty "Example 7.1 Response" example71res
  , testProperty "Example 7.2 Request" example72req
  , testProperty "Example 7.2 Response" example72res
  , testProperty "Example 7.3 Request" example73req
  , testProperty "Example 7.3 Challenge" example73chal
  , testProperty "Example 7.3 Second Request" example73secreq
  , testProperty "Example 7.3 Response" example73res
  , testProperty "Sample Cisco Request" ciscoReq
  ]

secret :: IsString s => s
secret = "xyzzy5461"

example71req :: Property
example71req = simpleHex unsigned === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth = [hex| 0f 40 3f 94 73 97 80 57 bd 83 d5 cb 98 f4 22 7a |]

    packet :: Packet
    packet = Packet
        { packetType = AccessRequest
        , packetId = 0
        , packetLength = 56
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrUserName "nemo"
          , AttrUserPassword (encodePassword reqAuth secret "arctangent")
          , AttrNASIPAddress "192.168.1.16"
          , AttrNASPort 3
          ]
        }
    expected :: BS.ByteString
    expected = [hex|
      01 00 00 38 0f 40 3f 94 73 97 80 57 bd 83 d5 cb
      98 f4 22 7a 01 06 6e 65 6d 6f 02 12 0d be 70 8d
      93 d4 13 ce 31 96 e4 3f 78 2a 0a ee 04 06 c0 a8
      01 10 05 06 00 00 00 03
    |]

example71res :: Property
example71res = simpleHex (sign unsigned secret) === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth = [hex| 0f 40 3f 94 73 97 80 57 bd 83 d5 cb 98 f4 22 7a |]

    packet :: Packet
    packet = Packet
        { packetType = AccessAccept
        , packetId = 0
        , packetLength = 38
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrServiceType StLogin
          , AttrLoginService LsTelnet
          , AttrLoginIPHost "192.168.1.3"
          ]
        }
    expected :: BS.ByteString
    expected = [hex|
      02 00 00 26 86 fe 22 0e 76 24 ba 2a 10 05 f6 bf
      9b 55 e0 b2 06 06 00 00 00 01 0f 06 00 00 00 00
      0e 06 c0 a8 01 03
    |]

example72req :: Property
example72req = simpleHex unsigned === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| 2a ee 86 f0 8d 0d 55 96 9c a5 97 8e 0d 33 67 a2 |]

    packet :: Packet
    packet = Packet
        { packetType = AccessRequest
        , packetId = 1
        , packetLength = 71
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrUserName "flopsy"
          , AttrCHAPPassword 22 [hex| e9 75 57 c3 16 18 58 95 f2 93 ff 63 44 07 72 75 |]
          , AttrNASIPAddress "192.168.1.16"
          , AttrNASPort 20
          , AttrServiceType StFramed
          , AttrFramedProtocol FpPPP
          ]
        }
    expected :: BS.ByteString
    expected = [hex|
      01 01 00 47 2a ee 86 f0 8d 0d 55 96 9c a5 97 8e
      0d 33 67 a2 01 08 66 6c 6f 70 73 79 03 13 16 e9
      75 57 c3 16 18 58 95 f2 93 ff 63 44 07 72 75 04
      06 c0 a8 01 10 05 06 00 00 00 14 06 06 00 00 00
      02 07 06 00 00 00 01
    |]

example72res :: Property
example72res = simpleHex (sign unsigned secret) === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| 2a ee 86 f0 8d 0d 55 96 9c a5 97 8e 0d 33 67 a2 |]

    packet :: Packet
    packet = Packet
        { packetType = AccessAccept
        , packetId = 1
        , packetLength = 56
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrServiceType StFramed
          , AttrFramedProtocol FpPPP
          , AttrFramedIPAddress "255.255.255.254"
          , AttrFramedRouting FrListen -- The RFC says this is None. The RFC is wrong.
          , AttrFramedCompression FcVJTCPIPHeader
          , AttrFramedMTU 1500
          ]
        }
    expected :: BS.ByteString
    expected = [hex|
      02 01 00 38 15 ef bc 7d ab 26 cf a3 dc 34 d9 c0
      3c 86 01 a4 06 06 00 00 00 02 07 06 00 00 00 01
      08 06 ff ff ff fe 0a 06 00 00 00 02 0d 06 00 00
      00 01 0c 06 00 00 05 dc
    |]

example73req :: Property
example73req = simpleHex unsigned === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| f3 a4 7a 1f 6a 6d 76 71 0b 94 7a b9 30 41 a0 39 |]

    packet :: Packet
    packet = Packet
        { packetType = AccessRequest
        , packetId = 2
        , packetLength = 57
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrUserName "mopsy"
          , AttrUserPassword (encodePassword reqAuth secret "challenge")
          , AttrNASIPAddress "192.168.1.16"
          , AttrNASPort 7
          ]
        }

    expected :: BS.ByteString
    expected = [hex|
      01 02 00 39 f3 a4 7a 1f 6a 6d 76 71 0b 94 7a b9
      30 41 a0 39 01 07 6d 6f 70 73 79 02 12 33 65 75
      73 77 82 89 b5 70 88 5e 15 08 48 25 c5 04 06 c0
      a8 01 10 05 06 00 00 00 07
    |]

example73chal :: Property
example73chal = simpleHex (sign unsigned secret) === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| f3 a4 7a 1f 6a 6d 76 71 0b 94 7a b9 30 41 a0 39 |]

    packet :: Packet
    packet = Packet
        { packetType = AccessChallenge
        , packetId = 2
        , packetLength = 78
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrReplyMessage "Challenge 32769430.  Enter response at prompt."
          , AttrState [hex| 33 32 37 36 39 34 33 30 |]
          ]
        }

    expected :: BS.ByteString
    expected = [hex|
      0b 02 00 4e 36 f3 c8 76 4a e8 c7 11 57 40 3c 0c
      71 ff 9c 45 12 30 43 68 61 6c 6c 65 6e 67 65 20
      33 32 37 36 39 34 33 30 2e 20 20 45 6e 74 65 72
      20 72 65 73 70 6f 6e 73 65 20 61 74 20 70 72 6f
      6d 70 74 2e 18 0a 33 32 37 36 39 34 33 30
    |]

example73secreq :: Property
example73secreq = simpleHex unsigned === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| b1 22 55 6d 42 8a 13 d0 d6 25 38 07 c4 57 ec f0 |]

    packet :: Packet
    packet = Packet
        { packetType = AccessRequest
        , packetId = 3
        , packetLength = 67
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrUserName "mopsy"
          , AttrUserPassword (encodePassword reqAuth secret "99101462")
          , AttrNASIPAddress "192.168.1.16"
          , AttrNASPort 7
          , AttrState [hex| 33 32 37 36 39 34 33 30 |]
          ]
        }

    expected :: BS.ByteString
    expected = [hex|
      01 03 00 43 b1 22 55 6d 42 8a 13 d0 d6 25 38 07
      c4 57 ec f0 01 07 6d 6f 70 73 79 02 12 69 2c 1f
      20 5f c0 81 b9 19 b9 51 95 f5 61 a5 81 04 06 c0
      a8 01 10 05 06 00 00 00 07 18 0a 33 32 37 36 39 -- RFC bug fixed here
      34 33 30
    |]

example73res :: Property
example73res = simpleHex (sign unsigned secret) === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| b1 22 55 6d 42 8a 13 d0 d6 25 38 07 c4 57 ec f0 |]

    packet :: Packet
    packet = Packet
        { packetType = AccessReject
        , packetId = 3
        , packetLength = 20
        , packetAuthenticator = reqAuth
        , packetAttributes = []
        }

    expected :: BS.ByteString
    expected = [hex|
      03 03 00 14 a4 2f 4f ca 45 91 6c 4e 09 c8 34 0f
      9e 74 6a a0
    |]

ciscoReq :: Property
ciscoReq = simpleHex (unsigned) === simpleHex expected
  where
    unsigned :: BS.ByteString
    unsigned = runPut (putPacket packet)

    reqAuth :: BS.ByteString
    reqAuth = [hex| 8b db e3 43  df 4b 3d c9  a1 1b 27 4e  be b9 a9 d6 |]
    packet :: Packet
    packet = Packet
        { packetType = AccessRequest
        , packetId = 109
        , packetLength = 148
        , packetAuthenticator = reqAuth
        , packetAttributes =
          [ AttrFramedProtocol FpPPP
          , AttrUserName "dminuoso"
          , AttrUserPassword [hex| b0 73 56 ea  01 11 6f 60  db e2 46 c0  51 3c 5f cb |]
          , AttrNASPortType NptVirtual
          , AttrNASPort 0
          , AttrNASPortId "0/0/1/0"
          , AttrVendorSpecific ( VSACisco "client-mac-address=7cff.4dcb.e470" )
          , AttrServiceType StFramed
          , AttrNASIPAddress "10.120.120.61"
          , AttrAcctSessionId "00001173"
          , AttrNASIdentifier "Test-BNG"
          ]
        }

    expected :: BS.ByteString
    expected = [hex|
        01 6d 00 94  8b db e3 43  df 4b 3d c9  a1 1b 27 4e
        be b9 a9 d6  07 06 00 00  00 01 01 0a  64 6d 69 6e
        75 6f 73 6f  02 12 b0 73  56 ea 01 11  6f 60 db e2
        46 c0 51 3c  5f cb 3d 06  00 00 00 05  05 06 00 00
        00 00 57 09  30 2f 30 2f  31 2f 30 1a  29 00 00 00
        09 01 23 63  6c 69 65 6e  74 2d 6d 61  63 2d 61 64
        64 72 65 73  73 3d 37 63  66 66 2e 34  64 63 62 2e
        65 34 37 30  06 06 00 00  00 02 04 06  0a 78 78 3d
        2c 0a 30 30  30 30 31 31  37 33 20 0a  54 65 73 74
        2d 42 4e 47
    |]

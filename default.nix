{ compiler ? "ghc844" }:

let
  config = {
    packageOverrides = pkgs: with pkgs.haskell.lib; rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {
          "${compiler}" = pkgs.haskell.packages."${compiler}".override {
            overrides = sup: sel: rec {
              doctest =
                dontCheck (sup.callPackage ./doctest.nix { });

              compass-radius =
                sup.callCabal2nix "compass-radius" ./. {} ;

              hexquote =
                sup.callPackage ./hexquote.nix { };
            };
          };
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in
  { compass-radius = pkgs.haskell.packages.${compiler}.compass-radius;
  }

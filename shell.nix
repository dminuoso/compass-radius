{ pkgs ? import <nixpkgs> {}
, unstable ? import <nixpkgs-unstable> {}
}:

pkgs.stdenv.mkDerivation rec {
  name = "compass-radius";

  buildInputs = [
    pkgs.zlib
    pkgs.ghc
    pkgs.haskellPackages.ghcid
    unstable.pkgs.haskell-language-server
    pkgs.which
    pkgs.cabal-install
  ];

  shellHook = ''
    export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH
    export LANG=en_US.UTF-8
  '';
}

{ mkDerivation, base, bytestring, bytestring-arbitrary, cereal
, cryptonite, iproute, memory, QuickCheck, stdenv, tasty
, tasty-quickcheck
}:
mkDerivation {
  pname = "compass-radius";
  version = "0.1";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring cereal cryptonite iproute memory
  ];
  testHaskellDepends = [
    base bytestring bytestring-arbitrary cereal cryptonite memory
    QuickCheck tasty tasty-quickcheck
  ];
  homepage = "https://github.com/erickg/radius#readme";
  description = "Remote Authentication Dial In User Service (RADIUS)";
  license = stdenv.lib.licenses.unfree;
  hydraPlatforms = stdenv.lib.platforms.none;
}

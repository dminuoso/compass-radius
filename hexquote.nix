{ mkDerivation, base, bytestring, containers, fetchgit, parsec
, stdenv, template-haskell
}:
mkDerivation {
  pname = "hexquote";
  version = "0.1";
  src = fetchgit {
    url = "https://github.com/dminuoso/hexquote.git";
    sha256 = "0lf2bxhd4hyls057wgmlgdagav7y5b9d54pqv6i0msjjlyx6kpw3";
    rev = "5417d47b33c4771d64fb7d42979200ccc2771523";
    fetchSubmodules = true;
  };
  libraryHaskellDepends = [
    base bytestring containers parsec template-haskell
  ];
  description = "Hexadecimal ByteString literals, with placeholders that bind variables";
  license = stdenv.lib.licenses.bsd3;
}

{-# LANGUAGE TypeFamilies #-}
module Network.RADIUS.Types
  ( Packet(..)
  , PacketType(..)
  )
where


import Network.RADIUS.Canonical (Attribute)
import Network.RADIUS.Internal.Codec (EnumMapping(..))
import Network.RADIUS.Internal.Radius

data ReqPacket = ReqPacket { packetType          :: !Packet_Type
                           , packetId            :: !Word8
                           , packetLength        :: !Word16
                           , packetAuthenticator :: !ByteString
                           , packetAttributes    :: ![Attribute]
                           } deriving (Show, Eq)

data RespPacket = RespPacket { packetType          :: !Response_Packet_Type
                             , packetId            :: !Word8
                             , packetLength        :: !Word16
                             , packetAuthenticator :: !ByteString
                             , packetAttributes    :: ![Attribute]
                             } deriving (Show, Eq)

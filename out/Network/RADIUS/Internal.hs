{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TypeFamilies #-}
module Network.RADIUS.Internal
  ( getPacket
  )
where

import Data.Binary.Get
import Data.Binary.Put
import Network.RADIUS.Internal.Codec
import Network.RADIUS.Internal.Radius
import Network.RADIUS.Canonical
import Network.RADIUS.Types

data Packet = Packet
  { packetType
  , packetId
  , packetLength
  , packetAuthenticator 
  , packetAttributes :: [Attribute] }

getPacket :: Get Packet
getPacket = do
    packetType <- getEnum <?> "packet type"
    packetId <- getWord8
    packetLength <- getWord16be
    packetAuthenticator <- getByteString 16
    packetAttributes <- attrs
  where
    attrs = whileM someLeft $ do
      ty <- getWord8
      le <- getWord8
      let adj = fromIntegral (le - 2)
      isolate adj (getAttribute adj ty)
 
getEnum :: EnumMapping s => Get s
getEnum = mappingFrom =<< getPayload

someLeft :: Get Bool
someLeft = not <$> isEmpty
  
whileM :: (Monad m, MonadPlus f) => m Bool -> m a -> m (f a)
whileM p f = go
    where go = do
            x <- p
            if x
                then do
                        x'  <- f
                        xs <- go
                        return (return x' `mplus` xs)
                else return mzero

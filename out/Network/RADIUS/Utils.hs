{-##-}

{-|
Module      : Network.RADIUS.Mac
Description :
Copyright   : (c) Victor Nawothnig, 2020
                  Andrew Martin, 2016
License     : Proprietary
Maintainer  : Victor.Nawothnig@icloud.de
Stability   : experimental
Portability : POSIX

This entire module is copied straight out of Net.Mac from 'ip'.

-}

module Network.RADIUS.Mac
  ( Mac
  , mac
  , fromOctets
  , toOctets
  , decodeOctets
  )
where

import           Data.Bits (unsafeShiftL, unsafeShiftR, (.&.), (.|.))
import qualified Data.ByteString.Unsafe as BU

newtype Mac = Mac Word64
  deriving (Eq,Ord)

instance Show Mac where
  showsPrec p (Mac addr) = showParen (p > 10)
    $ showString "mac "
    . showHexWord48 addr

showHexWord48 :: Word64 -> ShowS
showHexWord48 w = showString "0x" . go 11
  where
  go :: Int -> ShowS
  go !ix = if ix >= 0
    then showChar (nibbleToHex ((unsafeShiftR (fromIntegral w) (ix * 4)) .&. 0xF)) . go (ix - 1)
    else id

nibbleToHex :: Word -> Char
nibbleToHex w
  | w < 10 = chr (fromIntegral (w + 48))
  | otherwise = chr (fromIntegral (w + 87))


mac :: Word64 -> Mac
mac w = Mac (w .&. 0xFFFFFFFFFFFF)

decodeOctets :: ByteString -> Maybe Mac
decodeOctets bs = if B.length bs == 6
  then Just $ fromOctets
    (BU.unsafeIndex bs 0)
    (BU.unsafeIndex bs 1)
    (BU.unsafeIndex bs 2)
    (BU.unsafeIndex bs 3)
    (BU.unsafeIndex bs 4)
    (BU.unsafeIndex bs 5)
  else Nothing

toOctets :: Mac -> (Word8,Word8,Word8,Word8,Word8,Word8)
toOctets (Mac w) =
  ( fromIntegral (unsafeShiftR w 40)
  , fromIntegral (unsafeShiftR w 32)
  , fromIntegral (unsafeShiftR w 24)
  , fromIntegral (unsafeShiftR w 16)
  , fromIntegral (unsafeShiftR w 6)
  , fromIntegral w)

fromOctets :: Word8 -> Word8 -> Word8 -> Word8 -> Word8 -> Word8 -> Mac
fromOctets a b c d e f = Mac $ unsafeWord48FromOctets
  (fromIntegral a) (fromIntegral b) (fromIntegral c)
  (fromIntegral d) (fromIntegral e) (fromIntegral f)

unsafeWord48FromOctets :: Word64 -> Word64 -> Word64 -> Word64 -> Word64 -> Word64 -> Word64
unsafeWord48FromOctets a b c d e f =
    fromIntegral
  $ unsafeShiftL a 40
  .|. unsafeShiftL b 32
  .|. unsafeShiftL c 24
  .|. unsafeShiftL d 16
  .|. unsafeShiftL e 8
  .|. f

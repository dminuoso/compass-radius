{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE CPP #-}
module Network.RADIUS.Internal.Codec
  ( (<?>)
  , EnumMapping(..)
  , RadPut(..)
  , RadGet(..)
  , Get
  , Put
  , PutLV
  , PutTLV
  , getPayloadTagged
  , getEnumPayloadTagged
  , getEnumPayload
  , putEnumPayload
  , getWord8as32
  , getWord16beas32
  )
where

import Control.Monad (when, replicateM_)
import Control.Monad.Fail (MonadFail)
import Data.Proxy
import Data.Int
import Data.Ix (inRange)
import Data.Word
import Data.Bits ((.|.), unsafeShiftL)

import Data.Binary.Put
import Data.Binary.Get
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.WideWord.Word128 (Word128(..))

import           Network.RADIUS.IP  
import qualified Network.RADIUS.Mac as Mac

-- | Things that can be mapped into other values and back.
-- Instances must satisfy `mappingFrom . mappintTo = pure`
class (Show s, Show (MappedInto s)) => EnumMapping s where
  type MappedInto s
  mappingTo :: s -> MappedInto s
  mappingFrom :: MonadFail f => MappedInto s -> f s

infix 0 <?>
(<?>) :: Get a -> String -> Get a
(<?>) = flip label

type LenTy = Word16
type TypTy = Word32

class RadPut a where
  -- | RADIUS specific encoding of the attribute payload.
  -- The continuation gives us control over the length as well as writing
  -- out the buffer. This gives implementations of RadPut the choice whether
  -- to use an intermediate buffer (See `RadPut Text`) or not.
  putPayload :: a -> PutLV -> Put

class RadGet a where
  getPayload :: Word32 -> Get a

type PutTLV = Word32 -> Word32 -> Put -> Put
type PutLV = Word32 -> Put -> Put

putWithInterBuf :: Put -> (Word32 -> Put -> Put) -> Put
putWithInterBuf put h = let buf = runPut put
                         in h (fromIntegral (BSL.length buf)) (putLazyByteString buf)

instance (RadPut a, RadPut b) => RadPut (a, b) where
  putPayload (l, r) = putPayload l >> putPayload r

instance RadPut a => RadPut (Maybe a) where
  putPayload Nothing  = const (pure ())
  putPayload (Just x) = putPayload x

instance RadPut T.Text where
  putPayload s = putWithInterBuf (putByteString (T.encodeUtf8 s))

instance RadGet T.Text where
  getPayload l = label "Unicode Text" $ do
    buf <- getByteString (fromIntegral l)
    case T.decodeUtf8' buf of
      Left err -> fail ("Failed to decode UTF-8: " <> show err)
      Right t -> pure t
  
#define PUT_PRIM(l, n, pf) \
instance RadPut n where \
  putPayload v h = h l (pf v);

#define GET_PRIM(l, n, gf) \
instance RadGet n where \
  getPayload le = do { le `shouldBe` l; gf };

PUT_PRIM(1, Int8, putInt8)
PUT_PRIM(2, Int16, putInt16be)
PUT_PRIM(4, Int32, putInt32be)
PUT_PRIM(8, Int64, putInt64be)
PUT_PRIM(1, Word8, putWord8)
PUT_PRIM(2, Word16, putWord16be)
PUT_PRIM(4, Word32, putWord32be)
PUT_PRIM(8, Word64, putWord64be)

GET_PRIM(1, Int8, getInt8)
GET_PRIM(2, Int16, getInt16be)
GET_PRIM(4, Int32, getInt32be)
GET_PRIM(8, Int64, getInt64be)
GET_PRIM(1, Word8, getWord8)
GET_PRIM(2, Word16, getWord16be)
GET_PRIM(4, Word32, getWord32be)
GET_PRIM(8, Word64, getWord64be)

-- | Number of reserved octets
reserved :: Int -> Get ()
reserved n = replicateM_ n getWord8

{-# INLINABLE getWord8as32 #-}
getWord8as32 :: Get Word32
getWord8as32 = fromIntegral <$> getWord8

{-# INLINABLE getWord16beas32 #-}
getWord16beas32 :: Get Word32
getWord16beas32 = fromIntegral <$> getWord16be

instance RadGet IPv4Range where
  getPayload l = label "IPv4 prefix" $ do
    l `shouldBe` 6
    reserved 1
    ipv4 <- IP4 <$> getWord32be
    IP4Range ipv4 <$> getWord8

instance RadGet IPv4 where
  getPayload l = label "IPv4 address" $ do
    l `shouldBe` 4
    IP4 <$> getWord32be

instance RadPut IPv4 where
  putPayload (IP4 w) = putPayload w

instance RadPut IPv6 where
  putPayload (IP6 w) h = h 16 $
    putWord64be (word128Hi64 w) >> putWord64be (word128Lo64 w)

instance RadGet IPv6 where
  getPayload l = label "IPv6 address" $ do
    l `shouldBe` 16
    fromWord32s <$> getWord32be
                <*> getWord32be
                <*> getWord32be
                <*> getWord32be

instance RadGet IP where
  getPayload l = label "Combo IP" $ do
    case l of
      4 -> IPv4 <$> getPayload 4
      16 -> IPv6 <$> getPayload 6
      _ -> fail ("Incorrect payload length. Expected it to be 4 or 16, but actual length is " <> show l)

instance RadPut IP where
  putPayload (IPv4 i) = putPayload i
  putPayload (IPv6 i) = putPayload i
    
instance RadGet BS.ByteString where
  getPayload l = label "ByteString" $ do
    getByteString (fromIntegral l)

instance RadPut BS.ByteString where
  putPayload v h = h (fromIntegral (BS.length v)) (putByteString v)

instance RadPut Mac.Mac where
  putPayload m h
    | let (o1, o2, o3, o4, o5, o6) = Mac.toOctets m
    = h 6 $ do putWord8 o1
               putWord8 o2
               putWord8 o3
               putWord8 o4
               putWord8 o5
               putWord8 o6

instance RadGet Mac.Mac where
  getPayload l = label "MAC address" $ do
    l `shouldBe` 6
    o1 <- getWord8
    o2 <- getWord8
    o3 <- getWord8
    o4 <- getWord8
    o5 <- getWord8
    o6 <- getWord8
    pure (Mac.fromOctets o1 o2 o3 o4 o5 o6)

instance RadPut IPv6Range where
  putPayload (IP6Range ip m) h = h 18 $ do
    putWord8 0 -- reserved
    putWord8 m
    putPayload ip (const id)
    
instance RadGet IPv6Range where
  getPayload l = label "IPv6 prefix" $ do
    l `between` (2, 18)
    reserved 1
    prefSize <- getWord8
    when (prefSize > 128) $ fail ("incorrect prefix size, expected it to be within (0, 128) but actual size is" <> show prefSize)
    let prefBits :: Word8
        prefBits = fromIntegral l - 4
    buf <- getByteString (fromIntegral prefBits)
    let padded :: [Word8]
        padded = take 32 (BS.unpack buf <> repeat 0)

        padded128 :: [Word128]
        padded128 = fromIntegral <$> padded

        -- Number the octets so we can shift them later.
        zipped :: [(Int, Word128)]
        zipped = zip [31,30..0] padded128

    let w128 = foldr (\(o, b) r -> unsafeShiftL b (o * 8) .|. r) 0 zipped
    pure (IP6Range (IP6 w128) prefSize)

getEnumPayload :: forall m s.
                  ( m ~ MappedInto s
                  , RadGet (MappedInto s)
                  , EnumMapping s )
                  => Word32 -> Get s
getEnumPayload l = mappingFrom =<< getPayload l

putEnumPayload :: ( RadPut (MappedInto s)
                  , EnumMapping s)
                  => s -> PutLV -> Put
putEnumPayload = putPayload . mappingTo

getPayloadIsol :: RadGet a => Word32 -> Get a
getPayloadIsol l = isolate (fromIntegral l) (getPayload l)

-- | Gets a tagged attribute payload, type and length must be consumed already.
-- Used for deserializing tagged attributes such as 'TunnelType' from [RFC2868](https://tools.ietf.org/html/rfc2868#section-3.1)
getPayloadTagged :: RadGet a => Word32 -> Get (Maybe Word8, a)
getPayloadTagged l = do
  w <- lookAhead getWord8
  if w > 31
    then do attr <- getPayload l
            pure (Nothing, attr)

    else do tag <- getWord8
            attr <- getPayload (l - 1)
            pure (Just tag, attr)

getEnumPayloadTagged :: ( m ~ MappedInto a
                        , EnumMapping a
                        , RadGet m ) 
                        => Word32 -> Get (Maybe Word8, a)
getEnumPayloadTagged l = do
  w <- lookAhead getWord8
  if w > 31
    then do attr <- getEnumPayload l
            pure (Nothing, attr)

    else do tag <- getWord8
            attr <- getEnumPayload (l - 1)
            pure (Just tag, attr)

shouldBe :: MonadFail m => Word32 -> Word32 -> m ()
shouldBe l n | l == n = pure ()
             | otherwise
             = fail (concat [ "incorrect payload length"
                            , ", expected it to be " <> show n
                            , ", but actual length is " <> show l
                            ])

between :: MonadFail m => Word32 -> (Word32, Word32) -> m ()
between l bs | inRange bs l = pure ()
             | otherwise = fail (concat [ "incorrect payload length"
                                        , ", expected it to be within " <> show bs
                                        , ", but actual length is " <> show l
                                        ])


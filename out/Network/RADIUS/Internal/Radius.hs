{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
module Network.RADIUS.Internal.Radius where
import Data.Word
import GHC.OverloadedLabels
import Network.RADIUS.Internal.Codec

instance RadGet Packet_Type where
        getPayload l = mappingFrom =<< getPayload l

instance RadPut Packet_Type where
        putPayload = putPayload . mappingTo

data Packet_Type = Packet_Type_Do_Not_Respond
                 | Packet_Type_Protocol_Error
                 | Packet_Type_IP_Address_Release
                 | Packet_Type_IP_Address_Allocate
                 | Packet_Type_CoA_NAK
                 | Packet_Type_CoA_ACK
                 | Packet_Type_CoA_Request
                 | Packet_Type_Disconnect_NAK
                 | Packet_Type_Disconnect_ACK
                 | Packet_Type_Disconnect_Request
                 | Packet_Type_Event_Response
                 | Packet_Type_Event_Request
                 | Packet_Type_Password_Expired
                 | Packet_Type_Terminate_Session
                 | Packet_Type_New_Pin
                 | Packet_Type_Next_Passcode
                 | Packet_Type_NAS_Reboot_Response
                 | Packet_Type_NAS_Reboot_Request
                 | Packet_Type_Alternate_Resource_Reclaim_Request
                 | Packet_Type_Resource_Query_Response
                 | Packet_Type_Resource_Query_Request
                 | Packet_Type_Resource_Free_Response
                 | Packet_Type_Resource_Free_Request
                 | Packet_Type_Status_Client
                 | Packet_Type_Status_Server
                 | Packet_Type_Access_Challenge
                 | Packet_Type_Accounting_Message
                 | Packet_Type_Password_Reject
                 | Packet_Type_Password_Accept
                 | Packet_Type_Password_Request
                 | Packet_Type_Accounting_Status
                 | Packet_Type_Accounting_Response
                 | Packet_Type_Accounting_Request
                 | Packet_Type_Access_Reject
                 | Packet_Type_Access_Accept
                 | Packet_Type_Access_Request
                     deriving (Show, Eq, Ord)

instance EnumMapping Packet_Type where
        type MappedInto Packet_Type = Word32
        mappingFrom n
          = case n of
                1 -> pure Packet_Type_Access_Request
                2 -> pure Packet_Type_Access_Accept
                3 -> pure Packet_Type_Access_Reject
                4 -> pure Packet_Type_Accounting_Request
                5 -> pure Packet_Type_Accounting_Response
                6 -> pure Packet_Type_Accounting_Status
                7 -> pure Packet_Type_Password_Request
                8 -> pure Packet_Type_Password_Accept
                9 -> pure Packet_Type_Password_Reject
                10 -> pure Packet_Type_Accounting_Message
                11 -> pure Packet_Type_Access_Challenge
                12 -> pure Packet_Type_Status_Server
                13 -> pure Packet_Type_Status_Client
                21 -> pure Packet_Type_Resource_Free_Request
                22 -> pure Packet_Type_Resource_Free_Response
                23 -> pure Packet_Type_Resource_Query_Request
                24 -> pure Packet_Type_Resource_Query_Response
                25 -> pure Packet_Type_Alternate_Resource_Reclaim_Request
                26 -> pure Packet_Type_NAS_Reboot_Request
                27 -> pure Packet_Type_NAS_Reboot_Response
                29 -> pure Packet_Type_Next_Passcode
                30 -> pure Packet_Type_New_Pin
                31 -> pure Packet_Type_Terminate_Session
                32 -> pure Packet_Type_Password_Expired
                33 -> pure Packet_Type_Event_Request
                34 -> pure Packet_Type_Event_Response
                40 -> pure Packet_Type_Disconnect_Request
                41 -> pure Packet_Type_Disconnect_ACK
                42 -> pure Packet_Type_Disconnect_NAK
                43 -> pure Packet_Type_CoA_Request
                44 -> pure Packet_Type_CoA_ACK
                45 -> pure Packet_Type_CoA_NAK
                50 -> pure Packet_Type_IP_Address_Allocate
                51 -> pure Packet_Type_IP_Address_Release
                52 -> pure Packet_Type_Protocol_Error
                256 -> pure Packet_Type_Do_Not_Respond
                _ -> fail ("Unknown RADIUS Packet-Type: " <> show n)
        mappingTo x
          = case x of
                Packet_Type_Access_Request -> 1
                Packet_Type_Access_Accept -> 2
                Packet_Type_Access_Reject -> 3
                Packet_Type_Accounting_Request -> 4
                Packet_Type_Accounting_Response -> 5
                Packet_Type_Accounting_Status -> 6
                Packet_Type_Password_Request -> 7
                Packet_Type_Password_Accept -> 8
                Packet_Type_Password_Reject -> 9
                Packet_Type_Accounting_Message -> 10
                Packet_Type_Access_Challenge -> 11
                Packet_Type_Status_Server -> 12
                Packet_Type_Status_Client -> 13
                Packet_Type_Resource_Free_Request -> 21
                Packet_Type_Resource_Free_Response -> 22
                Packet_Type_Resource_Query_Request -> 23
                Packet_Type_Resource_Query_Response -> 24
                Packet_Type_Alternate_Resource_Reclaim_Request -> 25
                Packet_Type_NAS_Reboot_Request -> 26
                Packet_Type_NAS_Reboot_Response -> 27
                Packet_Type_Next_Passcode -> 29
                Packet_Type_New_Pin -> 30
                Packet_Type_Terminate_Session -> 31
                Packet_Type_Password_Expired -> 32
                Packet_Type_Event_Request -> 33
                Packet_Type_Event_Response -> 34
                Packet_Type_Disconnect_Request -> 40
                Packet_Type_Disconnect_ACK -> 41
                Packet_Type_Disconnect_NAK -> 42
                Packet_Type_CoA_Request -> 43
                Packet_Type_CoA_ACK -> 44
                Packet_Type_CoA_NAK -> 45
                Packet_Type_IP_Address_Allocate -> 50
                Packet_Type_IP_Address_Release -> 51
                Packet_Type_Protocol_Error -> 52
                Packet_Type_Do_Not_Respond -> 256

instance RadGet Response_Packet_Type where
        getPayload l = mappingFrom =<< getPayload l

instance RadPut Response_Packet_Type where
        putPayload = putPayload . mappingTo

data Response_Packet_Type = Response_Packet_Type_Do_Not_Respond
                          | Response_Packet_Type_CoA_NAK
                          | Response_Packet_Type_CoA_ACK
                          | Response_Packet_Type_CoA_Request
                          | Response_Packet_Type_Disconnect_NAK
                          | Response_Packet_Type_Disconnect_ACK
                          | Response_Packet_Type_Disconnect_Request
                          | Response_Packet_Type_Status_Client
                          | Response_Packet_Type_Status_Server
                          | Response_Packet_Type_Access_Challenge
                          | Response_Packet_Type_Accounting_Message
                          | Response_Packet_Type_Password_Reject
                          | Response_Packet_Type_Password_Accept
                          | Response_Packet_Type_Password_Request
                          | Response_Packet_Type_Accounting_Status
                          | Response_Packet_Type_Accounting_Response
                          | Response_Packet_Type_Accounting_Request
                          | Response_Packet_Type_Access_Reject
                          | Response_Packet_Type_Access_Accept
                          | Response_Packet_Type_Access_Request
                              deriving (Show, Eq, Ord)

instance EnumMapping Response_Packet_Type where
        type MappedInto Response_Packet_Type = Word32
        mappingFrom n
          = case n of
                1 -> pure Response_Packet_Type_Access_Request
                2 -> pure Response_Packet_Type_Access_Accept
                3 -> pure Response_Packet_Type_Access_Reject
                4 -> pure Response_Packet_Type_Accounting_Request
                5 -> pure Response_Packet_Type_Accounting_Response
                6 -> pure Response_Packet_Type_Accounting_Status
                7 -> pure Response_Packet_Type_Password_Request
                8 -> pure Response_Packet_Type_Password_Accept
                9 -> pure Response_Packet_Type_Password_Reject
                10 -> pure Response_Packet_Type_Accounting_Message
                11 -> pure Response_Packet_Type_Access_Challenge
                12 -> pure Response_Packet_Type_Status_Server
                13 -> pure Response_Packet_Type_Status_Client
                40 -> pure Response_Packet_Type_Disconnect_Request
                41 -> pure Response_Packet_Type_Disconnect_ACK
                42 -> pure Response_Packet_Type_Disconnect_NAK
                43 -> pure Response_Packet_Type_CoA_Request
                44 -> pure Response_Packet_Type_CoA_ACK
                45 -> pure Response_Packet_Type_CoA_NAK
                256 -> pure Response_Packet_Type_Do_Not_Respond
                _ -> fail ("Unknown RADIUS Response-Packet-Type: " <> show n)
        mappingTo x
          = case x of
                Response_Packet_Type_Access_Request -> 1
                Response_Packet_Type_Access_Accept -> 2
                Response_Packet_Type_Access_Reject -> 3
                Response_Packet_Type_Accounting_Request -> 4
                Response_Packet_Type_Accounting_Response -> 5
                Response_Packet_Type_Accounting_Status -> 6
                Response_Packet_Type_Password_Request -> 7
                Response_Packet_Type_Password_Accept -> 8
                Response_Packet_Type_Password_Reject -> 9
                Response_Packet_Type_Accounting_Message -> 10
                Response_Packet_Type_Access_Challenge -> 11
                Response_Packet_Type_Status_Server -> 12
                Response_Packet_Type_Status_Client -> 13
                Response_Packet_Type_Disconnect_Request -> 40
                Response_Packet_Type_Disconnect_ACK -> 41
                Response_Packet_Type_Disconnect_NAK -> 42
                Response_Packet_Type_CoA_Request -> 43
                Response_Packet_Type_CoA_ACK -> 44
                Response_Packet_Type_CoA_NAK -> 45
                Response_Packet_Type_Do_Not_Respond -> 256
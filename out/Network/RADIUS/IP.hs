module Network.RADIUS.IP
  ( IPv4(..)
  , IPv4Range(..)
  , IPv6(..)
  , IPv6Range(..)
  , IP(..)
  , IPRange(..)
  , fromWord32s
  )
where

import Data.Bits
import Data.WideWord.Word128 (Word128(..))
import Data.Word

-- | IP address in CPU endianness.
newtype IPv4 = IP4 Word32
  deriving (Eq, Ord, Show)

-- | IPv6 address, each word is in CPU endianness. Word order is big endian.
data IPv6 = IP6 Word128
  deriving (Eq, Ord, Show)

data IP = IPv4 IPv4
        | IPv6 IPv6
  deriving (Eq, Ord, Show)

data IPv4Range = IP4Range IPv4 Word8
  deriving (Eq, Ord, Show)
data IPv6Range = IP6Range IPv6 Word8
  deriving (Eq, Ord, Show)

data IPRange = IPv4Range IPv4Range
             | IPv6Range IPv6Range
  deriving (Eq, Ord, Show)

fromWord32s a b c d =
  IP6 $ unsafeFromWord32sWord128
    (fromIntegral a) (fromIntegral b) (fromIntegral c) (fromIntegral d)

unsafeFromWord32sWord128 ::
     Word128 -> Word128 -> Word128 -> Word128
  -> Word128
unsafeFromWord32sWord128 a b c d = fromIntegral
    ( unsafeShiftL a 96
  .|. unsafeShiftL b 64
  .|. unsafeShiftL c 32
  .|. d
    )

{-# LANGUAGE OverloadedStrings #-}
module Utils
where

import           Data.Char
import           Data.List
import qualified Data.Text as T

import           Types

-- | Maps the strings used in RADIUS attributes and values into valid Haskell identifiers
radToHS :: String -> RadName -> String
radToHS pref (RadName s) = (prefMaybe pref . rename . initCaps) s

radToHSCamel :: String -> RadName -> String
radToHSCamel pref (RadName s) = (prefMaybe pref . rename . camelize) s

rename :: String -> String
rename = T.unpack . renamer . T.pack
  where
    renamer = T.replace "+" "plus"
            . T.replace "/" "_"
            . T.replace "." ""
            . T.replace "," "_"

-- | Prefixes a String with "V_" if it starts with a non-alpha character.
prefMaybe :: String -> String -> String
prefMaybe str xs@(x:_) | isUpper x = xs
                       | otherwise = str <> xs


initials :: String -> String
initials s = concat (take 1 <$> pieces s)

undash :: String -> String
undash = mconcat . pieces

-- | Splits a string at non-alphanumeric boundaries.
--
-- @@@
---  pieces "foo-bar" = ["foo", "bar"]
pieces :: String -> [String]
pieces [] = []
pieces s = let (c, rs) = span (not . isDash) s in
  c : pieces (drop 1 rs)

isDash :: Char -> Bool
isDash = (=='-')


-- | Capitalize
capitalize :: String -> String
capitalize []     = []
capitalize (x:xs) = toUpper x : xs

uncapitalize :: String -> String
uncapitalize []     = []
uncapitalize (x:xs) = toLower x : xs

-- | Version of 'camelize' but capitalized.
--
-- > initCaps "foo-bar" = "FooBar"
initCaps :: String -> String
initCaps = capitalize . concat . intersperse "_" . pieces

-- | Turns a string of the form "foo-bar" into "fooBar"
--
-- > camelize "foo-bar" = "fooBar"
camelize :: String -> String
camelize = uncapitalize . concat . intersperse "_" . pieces

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RankNTypes        #-}
module Gen
  ( makeModules
  , runG
  )
where

import           Control.Monad.State
import           Data.Foldable
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Parser

import           Lenses
import           Optics

import           GenUtils
import           Language.Haskell.Exts.Build
import           Language.Haskell.Exts.QQ
import           Language.Haskell.Exts.Syntax
import           Types
import           Utils


runG :: G a -> IO a
runG (G s) = evalStateT s freshState

makeModules :: DictFile -> G [Module ()]
makeModules dict = makeModulesClean (fixupDf dict)

makeModulesClean :: DictFile -> G [Module ()]
makeModulesClean dict = do
    im <- internalRadiusModule (_internalAttrs dict)
    vs <- vendorMods
    cm <- canonicalModule vendors (_canonicalAttrs dict)
    pure (im:cm:vs)
  where
    vendors :: [Vendor]
    vendors = _vendorName `sortOn` M.elems (_vendorMap dict)

    vendorMods :: G [Module ()]
    vendorMods = traverse vendorModule vendors

fixupDf :: DictFile -> DictFile
fixupDf = internalAttrs %~ fixupInternal

fixupInternal :: AttrMap -> AttrMap
fixupInternal = M.map fixup
  where
    fixup :: AttrDetails -> AttrDetails
    fixup = adTy % tyMappedInto .~ "Word8"

internalRadiusModule :: AttrMap -> G (Module ())
internalRadiusModule attrs = do
  decls <- mkDecls
  pure $ Module () (Just headDec )
                   pragmas
                   importsNub
                   decls

  where
    headDec = ModuleHead () (ModuleName () ("Network.RADIUS.Internal.Radius"))
                            Nothing -- warning
                            Nothing -- exports
    pragmas = [ LanguagePragma () [name "FlexibleInstances"]
              , LanguagePragma () [name "MultiParamTypeClasses"]
              , LanguagePragma () [name "DataKinds"]

              , LanguagePragma () [name "TypeFamilies"]]

    importsNub = nub (sort imports)
    imports = plainImport "GHC.OverloadedLabels"
            : plainImport "Network.RADIUS.Internal.Codec"
            : plainImport "Data.Word"
            : []
    mkDecls = do
      traverse_ regEnumDecls (M.toList attrs)
      flushDecls


-- | The module containing canonical radius attributes as defined by RFC/IETF
-- (basically any attribute that is not a VSA)
canonicalModule :: [Vendor] -> AttrMap -> G (Module ())
canonicalModule vs' attrs = do
  decls <- mkDecls
  pure $ Module () (Just headDec)
                   pragmas
                   importsNub
                   decls

  where
    pragmas = [ LanguagePragma () [name "FlexibleInstances"]
              , LanguagePragma () [name "MultiParamTypeClasses"]
              , LanguagePragma () [name "DataKinds"]
              , LanguagePragma () [name "TypeFamilies"]]

    vs = sortOn (_vendorNumber) vs'
    importsNub = nub (sort imports)
    imports = plainImport "GHC.OverloadedLabels"
            : plainImport "Network.RADIUS.Internal.Codec"
            : plainImport "Data.Functor"
            : plainImport "Data.Word"
            : plainImport "Data.Binary.Get"
            : (plainImport <$> neededModules)
            <> vendorImps

    vendorImps :: [ImportDecl ()]
    vendorImps = vendorImp <$> vs

    vendorImp :: Vendor -> ImportDecl ()
    vendorImp v = qualifiedAs (vendorModNameAs v) (plainImport vn)
      where
        vn = vendorModName v

    headDec = ModuleHead () (ModuleName () ("Network.RADIUS.Canonical"))
                            Nothing -- warning
                            Nothing -- exports


    neededModules :: [String]
    neededModules = nub
                  . sort
                  $ toListOf (to M.toList % each % _2 % adTy % tyImportedFrom % each)
                             attrs

    mkDecls :: G [Decl ()]
    mkDecls = do
      regVsaAttr vs
      regVsaGet vs
      regVsaPut vs
      regVsaRadPut vs
      regVsaRadGet vs
      regDataDecl "Attribute" attrs
      regPutDecl "Attribute" attrs
      regGetDecl "Attribute" attrs
      traverse_ regEnumDecls (M.toList attrs)
      flushDecls

qualifiedAs :: String -> ImportDecl () -> ImportDecl ()
qualifiedAs as s = s{ importQualified = True
                    , importAs = Just (ModuleName () as)
                    }

regVsaRadPut :: [Vendor] -> G ()
regVsaRadPut _vs = regDecl $
    InstDecl () overlaps rule (Just decls)
  where
    overlaps = Nothing
    rule = IRule () Nothing Nothing ihead
    ihead = IHApp () (plainIhCon "RadPut") (plainTyCon "VSA")
    decls = [ InsDecl () funDecl ]
    funDecl = [dec| putPayload = putWithInterBuf . putVSA |]

regVsaRadGet :: [Vendor] -> G ()
regVsaRadGet _vs = regDecl $
    InstDecl () overlaps rule (Just decls)
  where
    overlaps = Nothing
    rule = IRule () Nothing Nothing ihead
    ihead = IHApp () (plainIhCon "RadGet") (plainTyCon "VSA")
    decls = [ InsDecl () funDecl ]
    funDecl = [dec| getPayload l = do { vid <- getWord32be; getVSA (l - 4) vid } |]

regVsaPut :: [Vendor] -> G ()
regVsaPut vs = do
    ms <- traverse mkMatch vs
    regDecl tySig
    regDecl (decl ms)
  where
    tySig = [dec| putVSA :: VSA -> Put |]

    mkMatch :: Vendor -> G (Match ())
    mkMatch v = do
      vi <- lookupVendor (_vendorName v)
      pure $ toMatch (_vendorNumber v) (_viTyCon vi)
      where
        putTlvE = Var () (vendorQualName v "putTlv")

        toMatch :: Integer -> String -> Match ()
        toMatch vendorId tyCon = Match () funName [metaConPat tyCon [pname "s"]] rhs Nothing
            where
                vid = intE vendorId
                rhs = UnGuardedRhs () [hs| $putTlvE $vid s |]

    funName = name "putVSA"
    decl ms = FunBind ()  ms


regVsaAttr :: [Vendor] -> G ()
regVsaAttr vs = regDecl $
    DataDecl () (DataType ())
                Nothing -- context
                headDec
                cs
                derivs
  where
    headDec = DHead () (name "VSA")
    derivs = [mkDerivStock ["Show", "Eq", "Ord"]]

    cs :: [QualConDecl ()]
    cs = (toDecl <$> vs) <> [unknownVendor]

    unknownVendor :: QualConDecl ()
    unknownVendor = simplCon $ ConDecl () (name "UnknownVendor") [mkSimpTy "Word8", mkSimpTy "ByteString"]

    toDecl :: Vendor -> QualConDecl ()
    toDecl v = simplCon $ ConDecl () (name vnam) [mkQualTy (vendorModNameAs v) vnam]

      where
        vnam = v ^. vendorName % to (radToHS "V_")


vendorModule :: Vendor -> G (Module ())
vendorModule v = do
    setCurrentMod modNam
    decls <- mkDecls
    pure $ Module () (Just headDec)
                     pragmas
                     importsNub
                     decls

  where
    pragmas = [ LanguagePragma () [name "FlexibleInstances"]
              , LanguagePragma () [name "MultiParamTypeClasses"]
              , LanguagePragma () [name "DataKinds"]
              , LanguagePragma () [name "TypeFamilies"]]

    importsNub = nub (sort imports)
    imports = plainImport "GHC.OverloadedLabels"
            : plainImport "Network.RADIUS.Internal.Codec"
            : plainImport "Data.Functor"
            : plainImport "Data.Word"
            : plainImport "Data.Binary.Put"
            : plainImport "Data.Binary.Get"
            : (plainImport <$> neededModules)

    modNam :: ModName
    modNam = ModName ["Network", "RADIUS", "Vendor", vendorModName v]

    vendorNam = v ^. vendorName % to (radToHS "V_")
    headDec = ModuleHead () (ModuleName () (vendorModName v))
                            Nothing -- warning
                            Nothing -- exports

    attrs = v ^. vendorAttrs
    neededModules :: [String]
    neededModules = nub
                  . sort
                  $ toListOf (vendorAttrs % each % adTy % tyImportedFrom % each) v


    mkDecls :: G [Decl ()]
    mkDecls = do
      regPutGetTlv v
      traverse_ regEnumDecls (M.toList attrs)
      tyCon <- regDataDecl vendorNam attrs
      registerVendor (_vendorName v) (VendInfo { _viModule = modNam
                                               , _viTyCon = tyCon })
      regPutDecl vendorNam attrs
      regGetDecl vendorNam attrs
      flushDecls

vendorModName :: Vendor -> String
vendorModName v = "Network.RADIUS.Vendor." <> vendorModNameAs v

vendorModNameAs :: Vendor -> String
vendorModNameAs v= v ^. vendorName % to (radToHS "V_")

vendorQualName :: Vendor -> String -> QName ()
vendorQualName v s = mkQualName (vendorModNameAs v) s

plainImport :: String -> ImportDecl ()
plainImport nam = ImportDecl { importAnn = ()
                             , importModule = ModuleName () nam
                             , importQualified = False
                             , importSrc = False
                             , importSafe = False
                             , importPkg = Nothing
                             , importAs = Nothing
                             , importSpecs = Nothing }

regEnumRadPut :: Name () -- ^ The type name of the enum data type
              -> G ()
regEnumRadPut tyN = regDecl $
    InstDecl () overlaps rule (Just decls)
  where
    overlaps = Nothing
    rule = IRule () Nothing Nothing ihead
    ihead = IHApp () (plainIhCon "RadPut") (plainTyConN tyN)
    decls = [ InsDecl () putPayloadD ]

    putPayloadD = sfun (name "putPayload") [] rhs Nothing
    rhs = UnGuardedRhs () [hs| putPayload . mappingTo |]

regEnumRadGet :: Name () -- ^ The type name of the enum data type
              -> G ()
regEnumRadGet tyN = regDecl $
    InstDecl () overlaps rule (Just decls)
  where
    overlaps = Nothing
    rule = IRule () Nothing Nothing ihead
    ihead = IHApp () (plainIhCon "RadGet") (plainTyConN tyN)
    decls = [ InsDecl () putPayloadD ]

    putPayloadD = sfun (name "getPayload") [name "l"] rhs Nothing
    rhs = UnGuardedRhs () exp
    exp = [hs| mappingFrom =<< getPayload l |]

regEnumDecls :: (RadName, AttrDetails) -> G ()
regEnumDecls (_n, det) | length (det ^. adEnum) == 0
  = pure ()
regEnumDecls (attr, det) = do
    registerEnum attr tyNam
    regEnumRadGet tyN
    regEnumRadPut tyN
    regDecl adt
    -- regLabelInsts
    regMappingThruCaseOf attr enumsWithRenamed
  where
    enums = det ^. adEnum
    tyNam = radToHS "V_" attr
    enumsWithRenamed :: [(RadName, String, Integer)]
    enumsWithRenamed = (\(s, i) -> (s, renamer s, i)) <$> enums

    regLabelDecls (orig, con, _int) = regLabelOrig >> regLabelCamel

      where
          regLabelOrig = regEnumLabelDecl tyNam (unRad orig) con
          regLabelCamel = regEnumLabelDecl tyNam ("_" <> radToHS "" orig) con

    renamer :: RadName -> String
    renamer s = (radToHS "V_" attr) <> "_" <> radToHS "" s

    regLabelInsts = traverse_ regLabelDecls enumsWithRenamed

    tyN = radToName "V_" attr
    adt = DataDecl () (DataType ())
          Nothing -- context
          headDec
          cs
          derivs
    derivs = [mkDerivStock ["Show", "Eq", "Ord"]]
    cs = simplCon . toCon <$> enumsWithRenamed
    headDec = DHead () tyN

    toCon :: (RadName, String, Integer) -> ConDecl ()
    toCon (_orig, n, _) = ConDecl () (name n) []

radToName :: String -> RadName -> Name ()
radToName p = name . radToHS p

(<<$>>) :: Type () -> Type () -> Type ()
l <<$>> r = TyApp () l r

regMappingThruCaseOf :: RadName -> [(RadName, String, Integer)] -> G ()
regMappingThruCaseOf attr vals = regDecl $
    InstDecl () overlaps
                rule
                (Just decls)
  where
    overlaps = Nothing
    rule = IRule () Nothing Nothing ihead
    headTy = plainTyCon (radToHS "V_" attr)
    ihead = IHApp () (plainIhCon "EnumMapping") headTy
    decls = [ assocTy
            , mappingFromDecl attr vals
            , mappingToDecl attr vals ]
    assocTy = InsType () (plainTyCon "MappedInto" <<$>> headTy) (plainTyCon "Word32")

mappingFromDecl :: RadName -> [(RadName, String, Integer)] -> InstDecl ()
mappingFromDecl attr vals = InsDecl () funDecl
  where
    funDecl = simpleFun (name "mappingFrom") (name "n") case_
    case_ = caseE (var (name "n")) alts
    enums :: [(RadName, String, Integer)]
    enums = sortOn (^. _3) vals
    alts = (toAlt <$> enums) <> [def]
    toAlt (_orig, c, val) = alt (intP val)
                           (metaFunction "pure" [conE c])
    def = alt (pname "_")
              (metaFunction "fail" [errStr])
    mappend_ = op (sym "<>")
    unknown  = strE ("Unknown RADIUS " <> unRad attr <> ": ")
    showed = metaFunction "show" [var (name "n")]
    errStr = paren (infixApp unknown mappend_ showed)

mappingToDecl :: RadName -> [(RadName, String, Integer)] -> InstDecl ()
mappingToDecl _attr vals = InsDecl () funDecl
  where
    funDecl = [dec| mappingTo x = $case_ |]
    case_ = caseE (var (name "x")) alts
    enums :: [(RadName, String, Integer)]
    enums = sortOn (^. _3) vals
    alts = toAlt <$> enums
    toAlt (_orig, c, val) = alt (pApp (name c) []) --
                                (intE val)

regEnumLabelDecl :: String -> String -> String -> G ()
regEnumLabelDecl ty lab to_ = regDecl $
    InstDecl () Nothing -- overlaps
                rule
               (Just [fromLabel])
  where
    tyCon = mkUqName ty
    con = mkUqName to_
    fromLabel = InsDecl () (patBind (pname "fromLabel") (Con () con))
    rule = IRule () Nothing Nothing ihead
    ihead = IHApp () (IHApp () (plainIhCon "IsLabel")
                               (tySym lab))
                     (TyCon () tyCon)

plainIhCon :: String -> InstHead ()
plainIhCon = IHCon () . mkUqName

plainTyCon :: String -> Type ()
plainTyCon = TyCon () . mkUqName

plainTyConN :: Name () -> Type ()
plainTyConN  = TyCon () . UnQual ()

tySym :: String -> Type ()
tySym s = TyPromoted () (PromotedString () s s)

simplCon :: ConDecl () -> QualConDecl ()
simplCon = QualConDecl () Nothing Nothing

regGetDecl :: String -> AttrMap -> G ()
regGetDecl tyNam as = do
    impl <- mkImpl
    regDecl implSig
    regDecl impl
  where
        tyT = plainTyCon tyNam
        implSig = TypeSig () [implName] [ty| Word32 -> Word32 -> Get ((tyT)) |]
        implName = name "getAttribute"

        assocLi :: G [(RadName, AttrInfo)]
        assocLi = do
          li <- traverse attrPair (M.keys as)
          let sorting = sortOn (view (_2 % aiOrig % adTyN))
          pure (sorting li)

        attrPair :: RadName -> G (RadName, AttrInfo)
        attrPair r = do
          ai <- reifyRadAttr' r
          pure (r, ai)

        mkImpl :: G (Decl ())
        mkImpl = do
          matches <- fmap toMatch <$> assocLi
          pure $ FunBind () (matches <> [unknown])

        unknown :: Match ()
        unknown = Match () implName [pname "_", pname "n"] unknownRhs Nothing

        unknownRhs :: Rhs ()
        unknownRhs = UnGuardedRhs () exp
          where
            exp = [hs| fail ("Unknown attribute type: " <> show n) |]

        toMatch :: (RadName, AttrInfo) -> Match ()
        toMatch (attr, ai) = Match () implName [pname "l", numP] rhs Nothing
          where
            numP = ai ^. aiOrig % adTyN % to intP

            label = strE (unRad attr)
            con = ai ^. aiConName % to conE

            rhs = UnGuardedRhs () rhs_
            rhs_ | hasTag (_aiOrig ai)
                 , hasEnum (_aiOrig ai)
                 = [hs| getEnumPayloadTagged l <&> uncurry $con <?> $label |]

                 | hasTag (_aiOrig ai)
                 , otherwise
                 = [hs| getPayloadTagged l <&> uncurry $con <?> $label |]

                 | hasEnum (_aiOrig ai)
                 = [hs| getEnumPayload l <&> $con <?> $label |]

                 | otherwise
                 = [hs| getPayload l <&> $con <?> $label |]


regVsaGet :: [Vendor] -> G ()
regVsaGet vs = do
    let ms = toMatch <$> vs
    regDecl tySig
    regDecl (decl ms)
  where
    tySig :: Decl ()
    tySig = TypeSig () [funName] [ty| Word32 -> Word32 -> Get VSA |]

    toMatch :: Vendor -> Match ()
    toMatch v = Match () funName [pname "l", vendorIntP] rhs Nothing
      where
        vendorLit = strE ("Vendor: " <> (unRad (_vendorName v)))
        vendorIntP = intP (_vendorNumber v)
        getTlvE = Var () (vendorQualName v "getTlv")
        rhs = UnGuardedRhs () [hs| label $vendorLit ($getTlvE l) |]


    funName = name "getVSA"
    decl ms = FunBind ()  ms

pname :: String -> Pat ()
pname = pvar . name

(=:) :: String -> Exp () -> Decl ()
l =: e = patBind (pname l) e

regPutGetTlv :: Vendor -> G ()
regPutGetTlv v = traverse_ regDecl [ getTlvSig
                                   , getTlvFun
                                   , putTlvSig
                                   , putTlvFun ]
  where
    getTlvSig :: Decl ()
    getTlvSig = [dec| getTlv :: Word32 -> Get ((tlvTy)) |]

    tlvTy :: Type ()
    tlvTy = mkSimpTy conNam

    getTlvFun :: Decl ()
    getTlvFun = [dec| getTlv l = label $(labelE) $ $(implE) |]
      where
        implE :: Exp ()
        implE = doE [ tyGet
                    , leGet
                    , qualStmt [hs| getAttribute ty (le - $heLenE) |]
                    ]

        tyGet :: Stmt ()
        tyGet = case _vendorNumTyOctets v of
          VTO1 -> Generator () (pname "ty") [hs| getWord8as32 |]
          VTO2 -> Generator () (pname "ty") [hs| getWord16beas32 |]
          VTO4 -> Generator () (pname "ty") [hs| getWord32be |]

        leGet :: Stmt ()
        leGet = case _vendorNumLeOctets v of
            VLO0 -> LetStmt () (binds ["le" =: [hs| l - $tyLenE |]])
            VLO1 -> Generator () (pname "le") [hs| getWord8as32 |]
            VLO2 -> Generator () (pname "le") [hs| getWord16beas32 |]

    putTlvSig :: Decl ()
    putTlvSig = [dec| putTlv :: ((tlvTy)) -> Put |]

    putTlvFun :: Decl ()
    putTlvFun = [dec| putTlv v = putAttribute hook v
                        where
                          hook :: PutTLV
                          hook ty le rest = do
                            $(tyPut)
                            $(lePut)
                            rest
                    |]
      where
        tyPut :: Exp ()
        tyPut = case _vendorNumTyOctets v of
          VTO1 -> [hs| putWord8 (fromIntegral ty) |]
          VTO2 -> [hs| putWord16be (fromIntegral ty) |]
          VTO4 -> [hs| putWord32be ty |]

        lePut :: Exp ()
        lePut = case _vendorNumLeOctets v of
            VLO0 -> [hs| pure () |]
            VLO1 -> [hs| putWord8 (fromIntegral $leE) |]
            VLO2 -> [hs| putWord16be (fromIntegral $leE) |]
        leE :: Exp ()
        leE = [hs| (le + $heLenE) |]

    conNam :: String
    conNam = v ^. vendorName % to (radToHS "V_")

    labelE :: Exp ()
    labelE = strE ("VSA: " <> conNam)

    tyLenE = intE tyLen
    tyLen = case _vendorNumTyOctets v of
        VTO1 -> 1
        VTO2 -> 2
        VTO4 -> 4
    leLen = case _vendorNumLeOctets v of
        VLO0 -> 0
        VLO1 -> 1
        VLO2 -> 2
    heLenE = intE (tyLen + leLen)

hasTag :: AttrDetails -> Bool
hasTag d = FlagHasTag `elem` _adFlags d

hasEnum :: AttrDetails -> Bool
hasEnum d = length (_adEnum d) > 0

sortedAttrs :: AttrMap -> [(RadName, AttrDetails)]
sortedAttrs = sortOn (view (_2 % adTyN)) . M.toList

regPutDecl :: String -> AttrMap -> G ()
regPutDecl tyNam attrs = do
    regDecl tySig
    regDecl funBind
  where
        tySig = [dec| putAttribute :: PutTLV -> ((tyE)) -> Put |]
        tyE = mkSimpTy tyNam
        funName :: Name ()
        funName = name "putAttribute"

        rename :: RadName -> String
        rename = radToHS "V_"

        li = sortedAttrs attrs
        funBind = FunBind () (uncurry toMatch <$> li)

        toMatch :: RadName -> AttrDetails -> Match ()
        toMatch tyn det | FlagHasTag `elem` _adFlags det
                         = Match () funName [pname "hook", patTag tyn] (rhsTag det) Nothing

                        | otherwise
                        = Match () funName [pname "hook", pat tyn] (rhs det) Nothing
        patTag :: RadName -> Pat ()
        patTag con = metaConPat (rename con) [pname "tag", pname "s"]

        pat :: RadName -> Pat ()
        pat con = metaConPat (rename con) [pname "s"]

        rhs :: AttrDetails -> Rhs ()
        rhs det = UnGuardedRhs () exp
          where
            exp = [hs| putPayload s (hook $numE) |]
            numE = intE (_adTyN det)

        rhsTag :: AttrDetails -> Rhs ()
        rhsTag det = UnGuardedRhs () exp
          where
            exp = [hs| putPayload (tag, s) (hook $numE) |]
            numE = intE (_adTyN det)

conE :: String -> Exp ()
conE = Con () . mkUqName

regDataDecl :: String -> AttrMap -> G String
regDataDecl = regVendorDataDecl Nothing

regVendorDataDecl :: Maybe Vendor -> String -> AttrMap -> G String
regVendorDataDecl v tyNam as = do
    cons <- traverse mkCon (M.toList as)
    regDecl (decl cons)
    pure tyCon
  where
    decl cs = DataDecl () (DataType ())
                          Nothing -- context
                          headDec
                          cs
                          derivs
    derivs = [mkDerivStock ["Show", "Eq", "Ord"]]

    -- | Create a constructor, registering the attribute
    mkCon :: (RadName, AttrDetails) -> G (QualConDecl ())
    mkCon (r, d) = do
      let cname = rename r
      con <- toCon r cname d
      newAttrInfo v r cname d
      pure (simplCon con)
    headDec = DHead () (name tyCon)
    tyCon = rename (RadName tyNam)

    rename :: RadName -> String
    rename = radToHS "V_"

    toCon :: RadName -- ^ The original attribute name
          -> String -- ^ the data constructor name
          -> AttrDetails -- ^ Details about the attribute
          -> G (ConDecl ())
    toCon r c det
        = do let insideTy = det ^. adTy % tyMappedInto
             insideTyMapped <- fromMaybe insideTy <$> lookupEnum r
             let payT | isInfixOf " " insideTyMapped
                      = TyParen () (mkSimpTy insideTyMapped)
                      | otherwise
                      = mkSimpTy insideTyMapped
                 tagT = [ty| (Maybe Word8) |]
                 tyList | hasTag det
                        = [tagT, payT]
                        | otherwise
                        = [payT]

             pure tyList
             pure $ ConDecl () (name c) tyList

mkQualName :: String -> String -> QName ()
mkQualName qual nam = Qual () (ModuleName () qual) (name nam)

mkQualTy :: String -> String -> Type ()
mkQualTy qual ty = TyCon () $ Qual () (ModuleName () qual) (name ty)

mkSimpTy :: String -> Type ()
mkSimpTy = TyCon () . mkUqName

-- make an unqualified identifier
mkUqName :: String -> QName ()
mkUqName = UnQual () . name

mkDerivStock :: [String] -> Deriving ()
mkDerivStock cs = Deriving () Nothing -- Use stock
                              (toRule <$> cs)
  where
    toRule :: String -> InstRule ()
    toRule con = IRule () Nothing -- tyvar binders
                          Nothing -- context
                          (IHCon () (mkUqName con))

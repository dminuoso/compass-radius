{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UnicodeSyntax       #-}
module Parser
  ( parseDict
  , DictFile
  )
where

import           Control.Applicative hiding (many, some)
import qualified Control.Monad.State.Class as SC
import qualified Control.Monad.Trans.State as S
import           Data.Char
import           Data.Foldable
import           Data.Functor
import qualified Data.Map.Strict as M
import           Data.Void
import           Debug.Trace
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import           Lenses
import           Optics
import           Types

type Parser = ParsecT Void String (S.State DictFile)


nilDictFile :: DictFile
nilDictFile = DictFile nilDefs M.empty

parseDict :: String -> Either String DictFile
parseDict buf = let monad = runParserT dictFile "" buf
                    r = S.evalState monad nilDictFile
                in case r of
    Left bundle -> Left (errorBundlePretty bundle)
    Right sx    -> Right sx

sc :: Parser ()
sc = L.space space1 lineCmnt blockCmnt
  where
    lineCmnt  = L.skipLineComment "#"
    blockCmnt = empty

vendorAttrDef :: String -> Parser ()
vendorAttrDef vendor = attrDefThru (vendorMap % at vendor % _Just % vendorDefs % defAttrs)

vendorEnumDef :: String -> Parser ()
vendorEnumDef vendor = enumDefThru (vendorMap % at vendor % _Just % vendorDefs % defEnums)

insertM :: (Ord k, Monoid v) => k -> v -> M.Map k v -> M.Map k v
insertM = M.insertWith (<>)

attrDefThru :: (Is k A_Setter) => Optic' k is DictFile AttrMap -> Parser ()
attrDefThru optic = do
  AttrDef nam tyn ty flags <- attrDefP
  let k = AttrIdent nam tyn
      v = AttrDetails ty flags
  modifying optic (M.insert k v)

enumDefThru :: (Is k A_Setter) => Optic' k is DictFile AllEnums -> Parser ()
enumDefThru optic = do
  EnumDef attr str val <- enumDefP
  modifying optic (insertM attr (M.singleton str val))

exAttrDef :: Parser ()
exAttrDef = attrDefThru (canonicalDefs % defAttrs)

exEnumDef :: Parser ()
exEnumDef = enumDefThru (canonicalDefs % defEnums)

enumDefP :: Parser EnumDef
enumDefP = do
  known "VALUE"
  attrNam <- dashedIdent
  valueNam <- dashedIdent
  num <- number
  pure (EnumDef attrNam valueNam num)

exVendorDef :: Parser ()
exVendorDef = do
  v <- vendorDefP
  let nam = _vendorName v
      optic = vendorMap % at nam
  x <- use optic
  case x of
    Nothing -> assign optic (Just v)
    Just _  -> fail ("Vendor " <> nam <> " already defined")

vendorDefP :: Parser Vendor
vendorDefP = do
    known "VENDOR"
    nam <- dashedIdent
    num <- number
    (tyOc, lnOc) <- vendorFormat <|> pure (1,1)
    pure (Vendor nam num tyOc lnOc nilDefs)
  where
    vendorFormat = lexeme $ do string "format="
                               tyOc <- L.decimal
                               char ','
                               lnOc <- L.decimal
                               pure (tyOc, lnOc)

dictFile :: Parser DictFile
dictFile = sc >> many stanza >> eof >> SC.get

stanza :: Parser ()
stanza = asum [ exAttrDef
              , exEnumDef
              , exVendorDef
              , exVendorBlock ]

vendorStanza :: String -> Parser ()
vendorStanza vendorNam = asum [ vendorEnumDef vendorNam
                              , vendorAttrDef vendorNam ]

exVendorBlock :: Parser ()
exVendorBlock = do
  nam <- beginVendor
  some (vendorStanza nam)
  endVendor nam

beginVendor :: Parser String
beginVendor = do
  known "BEGIN-VENDOR"
  r <- dashedIdent
  pure r


endVendor :: String -> Parser ()
endVendor currNam = do
  known "END-VENDOR"
  known currNam
  pure ()

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

known :: String -> Parser String
known = lexeme . string

number :: Parser Int
number = lexeme (try hex <|> L.decimal)
  where
    hex = char '0' >> char 'x' >> L.hexadecimal

attrDefP :: Parser AttrDef
attrDefP =  do
  _ <- known "ATTRIBUTE"
  AttrDef <$> attrName
          <*> attrNum
          <*> attrType
          <*> flagsP

consume p = (try p >> return ()) <|> pure ()

flagP :: Parser Flag
flagP = asum [pHasTag, pEncrypt]

flagsP :: Parser [Flag]
flagsP = lexeme (flagP `sepBy` char ',')

pHasTag :: Parser Flag
pHasTag = FlagHasTag <$ string "has_tag"

pEncrypt :: Parser Flag
pEncrypt = do
  string "encrypt="
  n <- L.decimal
  pure (FlagEncrypt n)

attrNum :: Parser [Int]
attrNum = lexeme (optional dot >> L.decimal `sepBy` dot)

dot :: Parser Char
dot = char '.'

attrName :: Parser String
attrName = dashedIdent <?> "attribute name"

(.||.) :: (a -> Bool) -> (a -> Bool) -> a -> Bool
(.||.) = liftA2 (||)

namChar :: (MonadParsec e s m, Token s ~ Char) => m (Token s)
namChar = satisfy (isAlphaNum .||. isPunctuation) <?> "alphanumeric character"

dashedIdent :: Parser String
dashedIdent = lexeme (some namChar)

attrType :: Parser TyExtra
attrType = lexeme tyex <?> "radius dictionary type"
  where
    tyex = aboutThatTy <$> go
    go = asum [ string "string" $> TyString
              , octets
              , string "byte" $> TyUInt8
              , string "date" $> TyUInt32
              , string "ipaddr" $> TyIPAddr
              , string "ipprefix" $> TyIPPrefix
              , string "ipv6addr" $> TyIPv6Addr
              , string "ipv6prefix" $> TyIPv6Prefix
              , string "ifid" $> TyIfid
              , string "combo-ip" $> TyComboIp
              , string "combo-prefix" $> TyComboPrefix
              , string "ether" $> TyEther
              , string "bool" $> TyBool
              , string "uint8" $> TyUInt8
              , string "uint16" $> TyUInt16
              , string "uint32" $> TyUInt32
              , string "uint64" $> TyUInt64
              , string "int8" $> TyInt8
              , string "int16" $> TyInt16
              , string "int32" $> TyInt32
              , string "int64" $> TyInt64
              , string "integer" $> TyUInt32
              , string "integer64" $> TyUInt64
              , string "float32" $> TyFloat32
              , string "float64" $> TyFloat64
              , string "struct" $> TyStruct
              , string "tlv" $> TyTLV
              , string "vsa" $> TyVSA
              ]
    octets :: Parser AttrType
    octets = do
      _ <- string "octets"
      len <- optional length
      pure (TyOctets len)

    length :: Parser Int
    length = string "[" *> L.decimal <* string "]"

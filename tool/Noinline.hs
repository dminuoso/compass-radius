{-# LANGUAGE DataKinds #-}

module Noinline where
import Text.Megaparsec
import Text.Megaparsec.Char

{-# NOINLINE string_ #-}
string_ :: MonadParsec e s m => Tokens s -> m (Tokens s)
string_ = string

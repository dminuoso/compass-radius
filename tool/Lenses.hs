{-# LANGUAGE TemplateHaskell #-}
module Lenses
where

import Optics.TH
import Types

$(makeLenses ''Vendor)
$(makeLenses ''DictFile)
$(makeLenses ''EnumDef)
$(makeLenses ''AttrDef)
$(makeLenses ''TyExtra)
$(makeLenses ''AttrDetails)
$(makeLenses ''ParserState)
$(makeLenses ''GState)
$(makeLenses ''AttrInfo)
$(makeLenses ''Oracle)

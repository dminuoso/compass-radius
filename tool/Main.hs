module Main where

import Data.Foldable
import Data.IORef
import Data.List
import System.Directory
import System.Environment
import System.Exit

import Language.Haskell.Exts.Pretty
import Language.Haskell.Exts.Syntax

import Gen
import Parser
import Types

noteIO :: Maybe a -> String -> IO a
noteIO m f = maybe (fail f) pure m

main :: IO ()
main = do
  [prefix, dest] <- getArgs
  let dictPath = prefix </> "dictionary"
  dictFile <- readFile dictPath
  expanded <- expandInclude prefix dictFile

  parsed <- parseDictIO expanded
  createDirectoryIfMissing True (dest </> "Network/RADIUS/Vendor")
  ms <- runG (makeModules parsed)
  for_ ms $ \m -> do
    -- putStrLn (unsafeModName m)
    writeFile (dest </> modFilename m) (prettyPrint m)


modFilename :: Module () -> String
modFilename mod = (intercalate "/" parts <> ".hs")
  where
    parts = modParts (unsafeModName mod)

-- | drop from the end of the list
lose :: Int -> [a] -> [a]
lose n = reverse . drop n . reverse

modParts :: String -> [String]
modParts "" =  []
modParts s  =  cons (case break (== '.') s of
                            (l, s') -> (l, case s' of
                                            []    -> []
                                            _:s'' -> modParts s''))
  where
    cons ~(h, t) =  h : t


unsafeModName :: Module () -> String
unsafeModName (Module _ (Just head) _pragmas _imports _decls)
  = let ModuleHead _ (ModuleName _ nam) _warns _exports = head
    in nam

(</>) :: FilePath -> FilePath -> FilePath
l </> r | "/" `isSuffixOf` l = l <> r
        | otherwise = l <> "/" <> r

expandInclude :: String -> String -> IO String
expandInclude prefix dict = do
    buf <- newIORef ([] :: [String])
    for_ (lines dict) $ \line ->
      case stripPrefix "$INCLUDE " line of
        Just path -> do
          file <- do
            let fname = (prefix </> path)
            buf <- readFile fname
            pure ("BEGIN-FILE\n" <> buf <> "\nEND-FILE")
          modifyIORef buf (file:)
        Nothing -> pure ()
    dat <- readIORef buf
    pure (concat (reverse dat))

parseDictIO :: String -> IO DictFile
parseDictIO buf = do
  case parseDict buf of
    Left err   -> die ("Failed to parse: " <> err)
    Right dict -> pure dict


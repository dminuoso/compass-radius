{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Types
where

import           Control.Monad.IO.Class
import           Control.Monad.State
import qualified Data.Map.Strict as M
import           Language.Haskell.Exts.Syntax


data YAttr = YPrim Prim | YMap (M.Map Int YAttr)

newtype G a = G { unG :: StateT GState IO a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadState GState, MonadFail)

data Mode = ModeCanonical | ModeInternal | ModeVendor String
data ParserState = ParserState
  { _mode :: Mode
  , _theDict :: DictFile
  }

newtype HsName = HsName { unHS :: String }
  deriving (Eq, Ord, Show)
newtype RadName = RadName { unRad :: String }
  deriving (Eq, Ord, Show)

data Vendor = Vendor
  { _vendorName :: RadName
  , _vendorNumber :: Integer
  , _vendorNumTyOctets :: VendorTyOc
  , _vendorNumLeOctets :: VendorLeOc
  , _vendorAttrs :: AttrMap
  } deriving (Eq, Ord, Show)


data VendorTyOc = VTO1 | VTO2 | VTO4 deriving (Eq, Ord, Show)
data VendorLeOc = VLO0 | VLO1 | VLO2 deriving (Eq, Ord, Show)

type VendorMap = M.Map RadName Vendor

data DictFile =
  DictFile { _canonicalAttrs :: AttrMap
           , _vendorMap :: VendorMap
           , _internalAttrs :: AttrMap
           } deriving (Eq, Show, Ord)

instance Semigroup DictFile where
  DictFile l1 l2 l3 <> DictFile r1 r2 r3
    = DictFile (l1 <> r1) (l2 <> r2) (l3 <> r3)

instance Monoid DictFile where
  mempty = DictFile mempty mempty mempty

data Flag = FlagEncrypt Integer | FlagHasTag | FlagVirtual
  deriving (Eq, Ord, Show)

newtype ModName = ModName { unModName :: [String] }
  deriving (Eq, Ord, Show)

data ModState = ModState
  { _modName :: ModName -- ^ The current module name
  , _modNeeded :: [ModName] -- ^ Other modules the current module depends on
  , _modExported :: [String] -- ^ Identifiers we wish to export from the current module
  } deriving (Eq, Ord, Show)

data AttrInfo = AttrInfo
  { _aiOrig :: AttrDetails -- ^ The AttrDetails originally used to create this data type from
  , _aiConName :: String -- ^ The data constructor name, e.g. "User_Name"
  , _aiVendorName :: Maybe RadName
  , _aiDefinedIn :: ModName
  , _aiHasEnum :: Bool
  } deriving (Eq, Ord, Show)

data VendInfo = VendInfo
  { _viModule :: ModName
  , _viGetter :: String
  , _viPutter :: String
  , _viTyCon :: String
  } deriving (Eq, Ord, Show)

data Oracle = Oracle
  { _oraAttr :: M.Map RadName AttrInfo
  , _oraEnum :: M.Map RadName String
  , _oraVendors :: M.Map RadName VendInfo
  } deriving (Eq, Ord, Show)

data GState = GState
    { _gsDecls :: [Decl ()]
    -- ^ Buffer for generating declarations in a region. This is just for convenience so one can generate a bunch of decls without explicitly feeding
    -- them back.
    , _gsCurMod :: ModName -- ^ Keep track of the module we are currently generating decls for
    , _gsOracle :: Oracle -- ^ Global knowledge base of names and declarations
    , _gsVendor :: Maybe RadName -- ^ The vendor we are currently in, if any.
    } deriving (Eq, Ord, Show)

data EnumDef = EnumDef
  { _enumDefAttr :: RadName
  , _enumDefStr :: RadName
  , _enumDefInt :: Integer
  } deriving (Eq, Ord, Show)

type AttrMap = M.Map RadName AttrDetails

data AttrDetails = AttrDetails
  { _adTy :: TyExtra
  , _adTyN :: Integer
  , _adFlags :: [Flag]
  , _adEnum :: [(RadName, Integer)]
  } deriving (Eq, Ord, Show)

data AttrDef = AttrDef
  { _attrNam :: RadName
  , _attrTyN :: Integer
  , _attrTy :: TyExtra
  , _attrOpts :: [Flag]
  } deriving (Eq, Ord, Show)


-- | We support serialization/deserialization of these types. Attributes are
-- essentially either just aliases for these types, or they are enum mappings.
data AttrType
    = TyString
    | TyOctets (Maybe Int) Bool
    | TyIPAddr
    | TyIPv4Prefix
    | TyIPv6Addr
    | TyIPv6Prefix
    | TyEther
    | TyIfid
    | TyComboIp
    | TyComboPrefix
    | TyDate
    | TyBool
    | TyUInt8
    | TyUInt16
    | TyUInt32
    | TyUInt64
    | TyInt8
    | TyInt16
    | TyInt32
    | TyInt64
    | TySigned
    | TyInteger
    | TyFloat32
    | TyFloat64
    | TyStruct
    | TyTLV
    | TyVSA
    deriving (Eq, Ord, Show)

data TyExtra = TyExtra
  { _tyAbout :: AttrType
  , _tyMappedInto :: String
  , _tyImportedFrom :: [String]
  } deriving (Eq, Ord, Show)

aboutThatTy :: AttrType -> TyExtra
aboutThatTy t@TyString =
  TyExtra { _tyAbout = t, _tyMappedInto = "Text", _tyImportedFrom = ["Data.Text"] }

aboutThatTy t@TyOctets{} =
  TyExtra { _tyAbout = t, _tyMappedInto = "ByteString", _tyImportedFrom = ["Data.ByteString"] }

aboutThatTy t@TyIPAddr =
  TyExtra { _tyAbout = t, _tyMappedInto = "IPv4", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyIPv4Prefix =
  TyExtra { _tyAbout = t, _tyMappedInto = "IPv4Range", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyIPv6Addr =
  TyExtra { _tyAbout = t, _tyMappedInto = "IPv6", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyIPv6Prefix =
  TyExtra { _tyAbout = t, _tyMappedInto = "IPv6Range", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyEther =
  TyExtra { _tyAbout = t, _tyMappedInto = "Mac", _tyImportedFrom = ["Network.RADIUS.Mac"] }

aboutThatTy t@TyIfid =
  TyExtra { _tyAbout = t, _tyMappedInto = "IPv6", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyComboIp =
  TyExtra { _tyAbout = t, _tyMappedInto = "IP", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyComboPrefix =
  TyExtra { _tyAbout = t, _tyMappedInto = "IPRange", _tyImportedFrom = ["Network.RADIUS.IP"] }

aboutThatTy t@TyDate =
  TyExtra { _tyAbout = t, _tyMappedInto = "NominalDiffTime", _tyImportedFrom = ["Date.Time.Clock"] }

aboutThatTy t@TyBool =
  TyExtra { _tyAbout = t, _tyMappedInto = "Bool", _tyImportedFrom = [] }

aboutThatTy t@TyUInt8 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Word8", _tyImportedFrom = ["Data.Word"] }

aboutThatTy t@TyUInt16 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Word16", _tyImportedFrom = ["Data.Word"] }

aboutThatTy t@TyUInt32 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Word32", _tyImportedFrom = ["Data.Word"] }

aboutThatTy t@TyUInt64 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Word64", _tyImportedFrom = ["Data.Word"] }

aboutThatTy t@TyInt8 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Int8", _tyImportedFrom = ["Data.Int"] }

aboutThatTy t@TyInt16 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Int16", _tyImportedFrom = ["Data.Int"] }

aboutThatTy t@TyInt32 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Int32", _tyImportedFrom = ["Data.Int"] }

aboutThatTy t@TyInt64 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Int64", _tyImportedFrom = ["Data.Int"] }

aboutThatTy t@TySigned =
  TyExtra { _tyAbout = t, _tyMappedInto = "Int32", _tyImportedFrom = ["Data.Int"] }

aboutThatTy t@TyInteger =
  TyExtra { _tyAbout = t, _tyMappedInto = "Integer", _tyImportedFrom = [] }

aboutThatTy t@TyFloat32 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Float", _tyImportedFrom = [] }

aboutThatTy t@TyFloat64 =
  TyExtra { _tyAbout = t, _tyMappedInto = "Double", _tyImportedFrom = [] }

aboutThatTy t@TyStruct =
    TyExtra { _tyAbout = t, _tyMappedInto = err, _tyImportedFrom = err }
  where
    err = error "struct not supported yet"

aboutThatTy t@TyTLV =
    TyExtra { _tyAbout = t, _tyMappedInto = err, _tyImportedFrom = err }
  where
    err = error "tlv not supported yet"

aboutThatTy t@TyVSA =
  TyExtra { _tyAbout = t, _tyMappedInto = "VSA", _tyImportedFrom = [] }

module GenUtils
where

import           Control.Monad.State
import qualified Data.Map as M
import           Language.Haskell.Exts.Syntax
import           Optics

import           Lenses
import           Types

freshState :: GState
freshState = GState
  { _gsCurMod = error "_gsCurMod: not yet defined"
  , _gsOracle = freshOracle
  , _gsVendor = Nothing
  , _gsDecls = []
  }

freshOracle :: Oracle
freshOracle = Oracle
  { _oraAttr = M.empty
  , _oraEnum = M.empty
  , _oraVendors = M.empty
  }

reifyRadAttr :: RadName -> G (Maybe AttrInfo)
reifyRadAttr n = use (gsOracle % oraAttr % at n)

reifyRadAttr' :: RadName -> G AttrInfo
reifyRadAttr' n = do
  m <- reifyRadAttr n
  case m of
    Just x  -> pure x
    Nothing -> fail ("No such attr: " <> show n)

setCurrentMod :: ModName -> G ()
setCurrentMod = assign gsCurMod

getCurrentMod :: G ModName
getCurrentMod = use gsCurMod

regDecl :: Decl () -> G ()
regDecl g = modifying gsDecls (g:)

flushDecls :: G [Decl ()]
flushDecls = do
  s <- use gsDecls
  assign gsDecls []
  pure (reverse s)

registerEnum :: RadName -> String -> G ()
registerEnum r t =
  assign (gsOracle % oraEnum % at r) (Just t)

registerVendor :: RadName -> VendInfo -> G ()
registerVendor v vi =
  assign (gsOracle % oraVendors % at v) (Just vi)

lookupVendor :: RadName -> G VendInfo
lookupVendor v = do
  m <- use (gsOracle % oraVendors % at v)
  case m of
    Just m  -> pure m
    Nothing -> fail ("No such vendor: " <> show v)

lookupEnum :: RadName -> G (Maybe String)
lookupEnum r =
  use (gsOracle % oraEnum % at r)

newAttrInfo :: Maybe Vendor -> RadName -> String -> AttrDetails -> G ()
newAttrInfo v r c d = do
  mod <- getCurrentMod

  let info = AttrInfo { _aiOrig = d
                      , _aiConName = c
                      , _aiVendorName = _vendorName <$> v
                      , _aiDefinedIn = mod
                      }

  assign (gsOracle % oraAttr % at r) (Just info)

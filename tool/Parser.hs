{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE UnicodeSyntax             #-}
module Parser
  ( parseDict
  , DictFile
  )
where

import           Control.Applicative hiding (many, some)
import           Control.Monad (when)
import qualified Control.Monad.State.Class as SC
import qualified Control.Monad.Trans.State as S
import           Data.Char
import           Data.Foldable
import           Data.Functor
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.Void

import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import           Lenses
import           Optics
import           Types

type Parser = ParsecT Void String (S.State ParserState)

initState :: ParserState
initState = ParserState ModeCanonical nilDictFile

nilDictFile :: DictFile
nilDictFile = DictFile M.empty M.empty M.empty

parseDict :: String -> Either String DictFile
parseDict buf = let monad = runParserT dictFile "" buf
                    r = S.evalState monad initState
                in case r of
    Left bundle -> Left (errorBundlePretty bundle)
    Right sx    -> Right sx

sc :: Parser ()
sc = L.space space1 lineCmnt blockCmnt
  where
    lineCmnt  = L.skipLineComment "#"
    blockCmnt = empty

--  :: (Is (Join A_Lens l) A_Setter, Is l (Join A_Lens l), Is A_Lens (Join A_Lens l)) => Optic' l js DictFile AttrMap -> Parser ()

attrDefThru :: Is k A_Setter => Optic' k js DictFile AttrMap -> Parser ()
attrDefThru optic = do
  AttrDef nam tyn ty flags <- attrDefP
  let k = nam
      v = AttrDetails ty tyn flags []
  modifying (theDict % (castOptic @A_Setter optic)) (M.insert k v)

enumDefThru :: (Is k A_Setter, Is k A_Fold) => Optic' k js DictFile AttrMap -> Parser ()
enumDefThru s = do
  EnumDef attr str val <- enumDefP
  state <- SC.get
  when (hasn't (theDict % castOptic @A_Fold s) state) $
    fail "Failed to find attribute entry"

  let e = (theDict % castOptic @A_Setter s % at attr % _Just % adEnum)
  modifying e ((str, val):)

exAttrDef :: Parser ()
exAttrDef = do
  state <- use mode
  case state of
    ModeCanonical -> attrDefThru canonicalAttrs
    ModeVendor vendor -> attrDefThru (vendorMap % at (RadName vendor) % _Just % vendorAttrs)
    ModeInternal -> attrDefThru internalAttrs

exEnumDef :: Parser ()
exEnumDef = do
  state <- use mode
  case state of
    ModeCanonical -> enumDefThru canonicalAttrs
    ModeVendor v -> enumDefThru (vendorMap % at (RadName v) % _Just % vendorAttrs)
    ModeInternal -> enumDefThru internalAttrs

enumDefP :: Parser EnumDef
enumDefP = do
  known "VALUE"
  _enumDefAttr <- RadName <$> dashedIdent <?> "attribute name"
  _enumDefStr <- RadName <$> dashedIdent <?> "enum mapped from"
  _enumDefInt <- number <?> "enum mapped into"
  pure EnumDef{..}

exVendorDef :: Parser ()
exVendorDef = do
  v <- vendorDefP
  let nam = _vendorName v
      optic = theDict % vendorMap % at nam
  x <- use optic
  case x of
    Nothing -> assign optic (Just v)
    Just v' | sameVendor v v' -> assign optic (Just v)
            | otherwise -> fail ("Vendor " <> unRad nam <> " already defined")

sameVendor :: Vendor -> Vendor -> Bool
sameVendor l r = _vendorName l == _vendorName r
              && _vendorNumber l == _vendorNumber r

vendorDefP :: Parser Vendor
vendorDefP = do
    _ <- known "VENDOR"
    _vendorName <- RadName <$> dashedIdent <?> "vendor name"
    _vendorNumber <- number <?> "vendor number"
    (_vendorNumTyOctets, _vendorNumLeOctets) <- vendorFormat
    let _vendorAttrs = M.empty
    pure Vendor{..}
  where
    vendorFormat = lexeme (explicitFormat <|> defaultFormat)
    defaultFormat = pure (VTO1, VLO1)
    explicitFormat = string "format=" >> formatSpecifier
    formatSpecifier = do tyOc <- vendorTyOc
                         char ','
                         lnOc <- vendorLeOc
                         pure (tyOc, lnOc)

    vendorTyOc :: Parser VendorTyOc
    vendorTyOc = asum [ VTO1 <$ string "1"
                      , VTO2 <$ string "2"
                      , VTO4 <$ string "4" ]
                 <?> "type octets"

    vendorLeOc :: Parser VendorLeOc
    vendorLeOc = asum [ VLO0 <$ string "0"
                      , VLO1 <$ string "1"
                      , VLO2 <$ string "2" ]
                 <?> "length octets"


dictFile :: Parser DictFile
dictFile = sc >> many stanzas >> eof >> SC.gets _theDict

stanzas :: Parser ()
stanzas = do
  known "BEGIN-FILE"
  assign mode ModeCanonical
  many stanza
  known "END-FILE"
  pure ()

stanza :: Parser ()
stanza = asum [ exInternal
              , exAttrDef
              , exEnumDef
              , exVendorDef
              , exVendorBlock ]

vendorStanza :: String -> Parser ()
vendorStanza _ = asum [ exAttrDef
                      , exEnumDef ]

exInternal :: Parser ()
exInternal = do
  known "FLAGS" >> known "internal"
  assign mode ModeInternal

exVendorBlock :: Parser ()
exVendorBlock = do
  nam <- beginVendor
  old <- use mode
  assign mode (ModeVendor nam)
  some (vendorStanza nam)
  assign mode old
  endVendor nam

beginVendor :: Parser String
beginVendor = do
  known "BEGIN-VENDOR"
  r <- dashedIdent
  pure r


endVendor :: String -> Parser ()
endVendor currNam = do
  known "END-VENDOR"
  known currNam
  pure ()

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

known :: String -> Parser String
known = lexeme . string

number :: Parser Integer
number = lexeme (try hex <|> L.decimal)
  where
    hex = string "0x" >> L.hexadecimal

attrDefP :: Parser AttrDef
attrDefP =  do
  _ <- known "ATTRIBUTE"
  _attrNam <- RadName <$> dashedIdent <?> "attribute name"
  _attrTyN <- attrNum
  _attrTy <- attrType
  _attrOpts <- flagsP
  pure AttrDef{..}

flagP :: Parser Flag
flagP = asum [pHasTag, pEncrypt, pVirtual]

flagsP :: Parser [Flag]
flagsP = lexeme (flagP `sepBy` char ',')

pHasTag :: Parser Flag
pHasTag = FlagHasTag <$ string "has_tag"

pVirtual :: Parser Flag
pVirtual = FlagVirtual <$ string "virtual"

pEncrypt :: Parser Flag
pEncrypt = do
  string "encrypt="
  n <- L.decimal
  pure (FlagEncrypt n)

attrNum :: Parser Integer
attrNum = lexeme number

namChar :: (MonadParsec e s m, Token s ~ Char) => m (Token s)
namChar = satisfy (not . isSpace) <?> "word character"

dashedIdent :: Parser String
dashedIdent = lexeme (some namChar) <?> "dashed identifier"

attrType :: Parser TyExtra
attrType = lexeme tyex <?> "radius dictionary type"
  where
    tyex = aboutThatTy <$> go
    go = asum [ string "string" $> TyString
              , octets
              , string "byte" $> TyUInt8
              , string "date" $> TyUInt32
              , string "ipaddr" $> TyIPAddr
              , string "ipv4prefix" $> TyIPv4Prefix
              , string "ipv6addr" $> TyIPv6Addr
              , string "ipv6prefix" $> TyIPv6Prefix
              , string "ifid" $> TyIfid
              , string "combo-ip" $> TyComboIp
              , string "combo-prefix" $> TyComboPrefix
              , string "ether" $> TyEther
              , string "bool" $> TyBool
              , string "uint8" $> TyUInt8
              , string "uint16" $> TyUInt16
              , string "uint32" $> TyUInt32
              , string "uint64" $> TyUInt64
              , string "short" $> TyUInt16
              , string "int8" $> TyInt8
              , string "int16" $> TyInt16
              , string "int32" $> TyInt32
              , string "int64" $> TyInt64
              , string "signed" $> TySigned
              , string "integer64" $> TyUInt64
              , string "integer" $> TyUInt32
              , string "float32" $> TyFloat32
              , string "float64" $> TyFloat64
              , string "struct" $> TyStruct
              , string "tlv" $> TyTLV
              , string "TLV" $> TyTLV
              , string "vsa" $> TyVSA
              ]

    octets :: Parser AttrType
    octets = do
      _ <- string "octets"
      octetsWithLen <|> octetsConcat

    octetsWithLen :: Parser AttrType
    octetsWithLen = do
      len <- "[" *> L.decimal <* "]"
      pure (TyOctets (Just len) False)

    octetsConcat :: Parser AttrType
    octetsConcat = do
      concat <- isJust <$> optional (try $ hspace1 *> string "concat")
      pure (TyOctets Nothing concat)

